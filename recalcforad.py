#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess
import numpy as np

path_qsub = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qsub"
sites = ["ontop","bridge","hollow"]
need_files = ["INCAR","KPOINTS","POSCAR","CONTCAR","POTCAR","XDATCAR","std.out","job.sh"]

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
# Read Sum_Eads.dat
path_sum_eads = path + "/Sum_Eads.dat"
f_se = open(path_sum_eads)
selines = f_se.readlines()
f_se.close()
del selines[0]

# Defnition of re-calc.
def recalc(path_recalc, prec):
    path_autosh = path_recalc + "/autorun.sh"
    path_calc   = path_recalc + "/calc"
    path_calc0  = path_recalc + "/calc0"
    path_clist  = path_recalc + "/c-list0"
    path_prec   = path_recalc + "/" + prec
    path_PrecM  = glob.glob(path_recalc+"/*PrecM*")[0]
    path_PrecN  = glob.glob(path_recalc+"/*PrecN*")[0]
    path_PrecA1 = glob.glob(path_recalc+"/*Ed1e-5Fd1e-2*")[0]
    path_PrecA2 = glob.glob(path_recalc+"/*Ed1e-6Fd1e-2*")[0]

    # Remove autorun.eo
    eo_autorun = [x for x in os.listdir(path_calc) if "autorun.e" in x or "autorun.o" in x]
    for eo in eo_autorun:
        path_eo = path_recalc + "/" + eo
        os.remove(eo)

    # Read c-list0
    f_cl = open(path_clist)
    clines = f_cl.readlines()
    f_cl.close()

    list_clist = [conv_line(x) for x in clines]
    if prec == "M":
        # remove Prec-directories
        shutil.rmtree(path_PrecN)
        shutil.rmtree(path_PrecA1)
        shutil.rmtree(path_PrecA2)

        # rename calc to calc0 and Prec to calc
        n = 0
        while True:
            path_calcN = path_recalc + "/calc" + str(n)
            if os.path.exists(path_calcN) == False:
                break
            n +=1

        os.rename(path_calc, path_calcN)
        os.rename(path_PrecM, path_calc)
        
    elif prec == "N":
        # remove Prec-directories
        shutil.rmtree(path_PrecA1)
        shutil.rmtree(path_PrecA2)

        # rename calc to calc0 and Prec to calc
        n = 0
        while True:
            path_calcN = path_recalc + "/calc" + str(n)
            if os.path.exists(path_calcN) == False:
                break
            n +=1

        os.rename(path_calc, path_calcN)
        os.rename(path_PrecN, path_calc)

        # change c-list0
        clines[1] = "# " + str(clines[1])
            
    elif prec == "A1":
        # remove Prec-directories
        shutil.rmtree(path_PrecA2)

        # rename calc to calc0 and Prec to calc
        n = 0
        while True:
            path_calcN = path_recalc + "/calc" + str(n)
            if os.path.exists(path_calcN) == False:
                break
            n +=1

        os.rename(path_calc, path_calcN)
        os.rename(path_PrecA1, path_calc)
        
        # change c-list0
        clines[1] = "# " + str(clines[1])
        clines[2] = "# " + str(clines[2])

    elif prec == "A2":
        # rename calc to calc0 and Prec to calc
        n = 0
        while True:
            path_calcN = path_recalc + "/calc" + str(n)
            if os.path.exists(path_calcN) == False:
                break
            n +=1

        os.rename(path_calc, path_calcN)
        os.rename(path_PrecA2, path_calc)

        # change c-list0
        clines[1] = "# " + str(clines[1])
        clines[2] = "# " + str(clines[2])
        clines[3] = "# " + str(clines[3])

    # make c-list0
    f_cl_n = open(path_clist,"w")
    for line in clines:
        f_cl_n.write(line)
    f_cl_n.close()

    # remove files in calc
    rmfiles = [x for x in os.listdir(path_calc) if x not in need_files]
    for rmfile in rmfiles:
        path_rmfile  = path_calc + "/" + rmfile
        os.remove(path_rmfile)
            
    # rename POSCAR and CONTCAR
    path_cont = path_calc + "/CONTCAR"
    path_pos  = path_calc + "/POSCAR"
    path_posini = path_calc + "/POSCAR_ini"
    path_pot = path_calc + "/POTCAR"
    path_job = path_calc + "/job.sh"

    if os.path.exists(path_cont) == True:
        os.rename(path_pos, path_posini)
        os.rename(path_cont, path_pos)

    if os.path.exists(path_pot) == False:
        shutil.copy(path_calc0+"/POTCAR",path_calc)
        
    if os.path.exists(path_job) == False:
        shutil.copy(path_calc0+"/job.sh",path_calc)
        
    os.chdir(path_recalc)
    qsub = path_qsub + " " + path_autosh
    subprocess.call(qsub,shell=True)
    os.chdir(path)
# End: #

list_se = [conv_line(x) for x in selines]
for site in list_se:
    print("\n" + site[0])
    path_dir_site = path + "/" + str(site[0])

    if str(site[2]) == "N":
        recalc(path_dir_site, "M")
        continue
    elif str(site[6]) == "N":
        recalc(path_dir_site, "N")
        continue
    elif str(site[10]) == "N":
        recalc(path_dir_site, "A1")
        continue
    elif str(site[14]) == "N":
        recalc(path_dir_site, "A2")
        continue

print("Done!")
        

