#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
import numpy as np
import addmol.helper as hl

jobname = input("\nPlease input the job name: ")
ch = "#$ -N"
nowpath = os.getcwd()
imgdirs = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" + x) and len(x) == 2])
path_tmp = nowpath + "/tmp"
path_files_tmp = [path_tmp + "/" + x for x in os.listdir(path_tmp)]

for imgdir in imgdirs:
    print(imgdir)
    path_img = nowpath + "/" + imgdir
    path_job = path_img + "/job.sh"

    for path_f in path_files_tmp: shutil.copy(path_f, path_img)
    with open(path_job) as fj: joblines = fj.readlines()

    for i, jobline in enumerate(joblines):
        if ch in jobline:
            joblines[i] = ch + " " + str(jobname) + "_" + str(imgdir) + "\n"

    Lj = "".join(joblines)
    with open(path_job, "w") as fj: fj.write(Lj)


    if os.path.exists(path_img  + "/OUTCAR"): shutil.move(path_img + "/OUTCAR", path_img + "/OUTCAR_b")

    os.chdir(path_img)
    sub.call("qsub -g tga-MCES01 job.sh", shell=True)

print("\nDONE!\n")
#END: script