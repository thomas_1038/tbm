#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2015/07/03 ver1.0
#usage:script A1 A2 ... Af (A1~Af:Pseudopotential name)
#This script makes POTCAR.

import os, sys, shutil, threading
import subprocess as sub
import addmol.helper as hl

def mk_pot(nowpath, argv=[""]):
    ############################### INPUT ZONE ###############################
    vaspver = "5.4"
    path_potlist = "/home/4/17D20121/script/tn_scripts/ref/pot_list"
    path_pp = "/home/4/17D20121/vasp/vasp_pot/potpaw_PBE.54"
    ############################### INPUT ZONE ###############################

    numJ_pos = 0  #; argv = sys.argv
    path_pot = nowpath + "/POTCAR"
    path_pos = nowpath + "/POSCAR"

    # read pot_list
    with open(path_potlist) as fpot: potlistlines = fpot.readlines()
    potlist = [hl.conv_line(x) for x in potlistlines]
    elements = [x[0] for x in potlist]
    pp = [x[1] for x in potlist]

    # read POSCAR & print Element labels
    if not os.path.exists(path_pos):
        if len(argv) == 1:
            print("\nYou should prepare POSCAR or input NAME of POCAR! BYE!\n"); sys.exit()

    elif len(argv) > 1: 
        pos_pp = argv[1:]
        Le = " ".join(pos_pp)

    else:
        numJ_pos = 1
        with open(path_pos) as fpos: poslines = fpos.readlines()

        pos_elements = hl.conv_line(poslines[5])
        pos_pp = []; Le = ""
        for el in pos_elements:
            if el in elements:
                pos_pp.append(pp[elements.index(el)])
                Le += "  " + el

    Lpp = ""
    if numJ_pos == 0:
        num1 = 1
        while num1 < len(argv):
            with open(path_pp + "/" + str(argv[num1]) +  "/POTCAR") as fpp: potline = fpp.read()
            Lpp += potline
            num1 += 1
    else:
        for elforpp in pos_pp:
            with open(path_pp + "/" + str(elforpp) +  "/POTCAR") as fpp: potline = fpp.read()
            Lpp += potline

    with open(path_pot, "w") as fpot: fpot.write(Lpp)

    Ls ="\n############################################\n" +\
         "#            POTCAR of VASP " + vaspver + ".           #\n" +\
          "############################################\n"
    Ls += "\n           Elements in POSCAR\n" + "               " + Le + "\n\n"
    Ls += "############################################\n\n"

    with open(path_pot) as fpp: lines = fpp.readlines()
    for line in lines:
        if line.find("PBE") >= 0: Ls += str(line[:-1]) + "\n"
    Ls += "\n############################################\n"
    print(Ls)

if __name__ == "__main__": 
    argv = sys.argv
    mk_pot(os.getcwd(), argv)
# END: script