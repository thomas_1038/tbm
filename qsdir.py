#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2018/06/22

import os, sys, shutil, threading, glob, re
import subprocess as sub
import datetime as dt
sys.path.append("/home/4/17D20121/script/tn_scripts")
from others import readdim as rd
from get import getmaxforce as gm

def qsdir():
    path_qstat = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qstat"
    nodes = ["f_node","h_node","q_node","s_gpu","q_core","s_core"]
    dict_nodes = {"f_node":"fn", "h_node":"hn", "q_node":"qn", "s_gpu":"sg", "q_core":"qc", "s_core":"sc"}
    dict_points = {"f_node":1.00, "h_node":0.50, "q_node":0.25, "s_gpu":0.20, "q_core":0.20, "s_core":0.06}
    dict_cores = {"f_node":28, "h_node":14, "q_node":7, "s_gpu":2, "q_core":4, "s_core":1}
    flag_req  = "reached required accuracy - stopping structural energy minimisation"
    flag_wav  = "writing wavefunctions"

    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def adjust_len(list_qd):
        list_new = []; list_new_ap = list_new.append
        max_len = max([len(x) for x in list_qd])
        L = ""
        for i in range(0,max_len): L += "-"
        for l in list_qd:
            str_num = max_len - len(l); n = 1
            while n < str_num +1: l += " "; n += 1
            list_new_ap(l)
        list_new.insert(1,L)
        return list_new

    def get_nsw_from_INCAR(path_INCAR):
        with open(path_INCAR) as fi: incarlines = fi.readlines()
        NSW = [x.replace("NSW = ","").replace("\n","") for x in incarlines if "NSW = " in x][0]
        return NSW

    def get_iter(path_re, NSW, num_d=1):
        num_iter = "1N0"; sign_iter = ""
        path_stdout = path_re + "/std.out"

        if os.path.exists(path_stdout):
            with open(path_stdout) as fs: stdline = fs.read()
            if flag_req in stdline: sign_iter = "R"
            elif flag_wav in stdline: sign_iter = "W"
            elif "F=" in stdline: sign_iter = "N"

            if not sign_iter == "": num_iter = str(num_d) + sign_iter + ":" + str(stdline.count("F=")) + "/" + NSW
            else: num_iter = str(num_d) + "N:0"  + "/" + NSW; sign_iter ="N"

        else: sign_iter = "N0"
        return num_iter, sign_iter

    def get_last_force(path_re):
        gm.get_maxforce(path_re)
        with open(path_re + "/MAXFORCE") as fm: mflines = fm.readlines()
        if len(mflines) != 0:
            last_f = [float(conv_line(x)[2]) for x in mflines][-1]; last_f = "%.3f" % last_f
            f_label = [conv_line(x)[3] for x in mflines][-1]
        else: last_f = "-----"; f_label = "---"
        return last_f, f_label

    def read_jobsh(work_dir, script_file):
        path_job = work_dir + "/" + script_file
        with open(path_job) as f: joblines = f.readlines()
        nodeline = [line for line in joblines if "#$ -l" in line and set(line.replace("=", " ").split(" ")) & set(nodes)][0].replace("=", " ").split(" ")
        nodetype = nodeline[2]
        num_node = int(nodeline[3].replace("\n",""))
        btime = conv_line([line for line in joblines if "h_rt=" in line][0])[2].replace("h_rt=","")
        if len(btime) == 7: btime = "0" + btime

        return nodetype, num_node, btime

    def calc_cost(utime_sec, btime, nodetype, numnode):
        point_eff = dict_points[nodetype]

        btime_list = btime.split(":")
        btime_sec = 3600 * int(btime_list[0]) + 60 * int(btime_list[1]) + int(btime_list[2])
        bpoint = round(numnode * point_eff*( 0.7 * float(btime_sec) + 0.1 * float(btime_sec) ),0)
        if utime_sec > 300: upoint = round(numnode*point_eff*( 0.7 * float(utime_sec) + 0.1 * float(btime_sec) ),0)
        else: upoint = round(numnode*point_eff*( 0.7 * 300.0 + 0.1 * float(btime_sec) ),0)
        byen = round(float(bpoint)/144,0); uyen = round(float(upoint)/144,0)

        return bpoint, upoint, byen, uyen

    now = dt.datetime.now()
    nowpath = os.getcwd()
    sub.call(path_qstat + " > qstat.dat", shell=True)
    path_qstatdat = nowpath + "/qstat.dat"
    with open(path_qstatdat) as fq: qstatlines = fq.readlines()
    os.remove(path_qstatdat)

    if len(qstatlines) == 0: sys.exit()
    del qstatlines[0]; del qstatlines[0]
    line_qstat = [conv_line(x) for x in qstatlines]

    jobID = ["job-ID"]; jobID_append = jobID.append
    job_state = ["S"]; job_state_append = job_state.append
    group = ["group"]; group_append = group.append
    job_name = ["job_name"]; job_name_append = job_name.append
    workdir = ["workdir"]; workdir_append = workdir.append
    node_type = ["node"]; node_type_append = node_type.append
#    slots = ["n"]; slots_append = slots.append
    booked_time = ["btime"]; booked_time_append = booked_time.append
    used_time = ["utime"]; used_time_append = used_time.append
    point = ["point"]; point_append = point.append
    booked_cost =["bcost"]; booked_cost_append = booked_cost.append
    used_cost = ["ucost"]; used_cost_append = used_cost.append
    iteration = ["num_iter"]; iteration_append = iteration.append

    # FORCE in relaxation
    maxforce = ["maxF"]; maxforce_append = maxforce.append
    force_atom = ["Atom"]; force_atom_append = force_atom.append

    # DIMER METHOD
    step_dim = ["step"]; step_dim_append = step_dim.append
    force_dim = ["force"]; force_dim_append = force_dim.append
    torque_dim = ["torque"]; torque_dim_append = torque_dim.append
    curvature_dim = ["curvature"]; curvature_dim_append = curvature_dim.append
    angle_dim = ["angle"]; angle_dim_append = angle_dim.append

    for line in line_qstat:
        num_iter = "1N0"; last_f = "-----"; Ea = "----"; f_label = "---"
        dimstep = "--"; dimforce = "-----"; torque = "-----"; curvature = "-----"; angle = "-----"


        job_id = line[0]

        # qstat -j ID
        sub.call(path_qstat + " -j " + job_id + " > qstatj.dat", shell=True)
        path_qstatj = nowpath + "/qstatj.dat"
        with open(path_qstatj) as fj: jlines = fj.readlines()
        os.remove(path_qstatj)

        # job_name
        job = conv_line([jline for jline in jlines if "job_name" in jline][0])[1]
        if job == "QRLOGIN": continue
        else: job_name_append(job); jobID_append(line[0]); state = line[4]

        if state == "qw":
            job_state_append(line[4])
            used_time_append("00:00:00")
            num_core = int(line[7])
            utime_sec = 0

        else:
            job_state_append("r ")

            # used_time
            st1 = line[5].split("/"); st2 = line[6].split(":")
            st = dt.datetime(int(st1[2]),int(st1[0]),int(st1[1]),int(st2[0]),int(st2[1]),int(st2[2]),0)
            utime = str(now - st).split(".")[0]; utime_list = utime.split(":")

            if "-1 day, 23" in utime_list: print(utime_list); sys.exit()
            utime_sec = 3600 * int(float(utime_list[0])) + 60 * int(float(utime_list[1])) + int(float(utime_list[2]))
            if len(utime) == 7: utime = "0" + utime
            used_time_append(utime)

            # num_core
            num_core = int(line[8])

        # qstat -j ID
        sub.call(path_qstat + " -j " + job_id + " > qstatj.dat", shell=True)
        path_qstatj = nowpath + "/qstatj.dat"
        with open(path_qstatj) as fj: jlines = fj.readlines()
        os.remove(path_qstatj)

        # work_dir
        work_dir = conv_line([jline for jline in jlines if "sge_o_workdir" in jline][0])[1]
        workdir_append(work_dir)

        # group name
        groupline = conv_line([jline for jline in jlines if "submit_cmd" in jline][0])
        if len(groupline) == 3: group_append("free")
        else: group_append(groupline[3])

        # script_file
        script_file = conv_line([jline for jline in jlines if "script_file" in jline][0])[1]

        # node_type, num_node, booked_time
        nodetype, num_node, btime = read_jobsh(work_dir, script_file)
        if nodetype == "s_gpu": node_type_append(node +"_" + str(int(num_core/dict_cores[nodetype])))
        else: node_type_append(dict_nodes[nodetype] + "_" + str(int(num_core/dict_cores[nodetype])))
        booked_time_append(btime)
        #slots_append(str(int(num_core/dict_cores[nodetype])))

        bpoint, upoint, byen, uyen = calc_cost(utime_sec, btime, nodetype, num_node)
        point_append(str(int(upoint))+"/"+str(int(bpoint)))
        booked_cost_append(str(int(bpoint))+"/"+str(int(byen)))
        used_cost_append(str(int(upoint))+"/"+str(int(uyen)))

        #print(work_dir)
        #
        vauto_in_dir = [x for x in os.listdir(work_dir) if x in "vauto-input"]
        if len(vauto_in_dir) > 0:
            path_vauto = work_dir + "/vauto-input"
            path_INCAR = work_dir + "/calc/INCAR"
            NSW = get_nsw_from_INCAR(path_INCAR)

            with open(path_vauto) as fv: vautolines = fv.readlines()
            redirs = [conv_line(x) for x in vautolines if len(conv_line(x)) > 0 and [v for v in conv_line(x) if v != "#"][0] in ["M","N","A"] and conv_line(x)[1] != "A"]
            redirs = [[x for x in list if x != "#"] for list in redirs]

            num_d = 1; num_iter ="1N:0/" + NSW
            for num_prec, x in enumerate(redirs):
                if str(x[8])=="F": dirname=str(num_prec)+"_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])
                elif str(x[8])=="T": dirname=str(num_prec)+"_Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])+"Fd"+str(x[9])
                path_redir_in_redirs = work_dir + "/" + dirname

                num_iter, sign_iter = get_iter(path_redir_in_redirs, NSW, num_d)
                if sign_iter != "N0": last_f, f_label = get_last_force(path_redir_in_redirs)
                if sign_iter == "N" or sign_iter == "N0": break
                num_d += 1

        else:
            path_INCAR = work_dir + "/INCAR"
            if os.path.exists(path_INCAR):
                NSW = get_nsw_from_INCAR(path_INCAR)
                num_iter, sign_iter = get_iter(work_dir, NSW, 1)
            else: num_iter = "---"

            path_dim = work_dir + "/DIMCAR"
            if os.path.exists(path_dim): dimstep, dimforce, torque, curvature, angle = rd.read_dimcar(work_dir)
            else: dimstep = "--"; dimforce = "-----"; torque = "-----"; curvature = "-----"; angle = "-----"

        iteration_append(num_iter); maxforce_append(last_f); force_atom_append(f_label)
        step_dim_append(dimstep); force_dim_append(dimforce); torque_dim_append(torque); curvature_dim_append(curvature); angle_dim_append(angle)
    # END LOOP

    argv = sys.argv
    if len(argv) == 1:
        list_for_loop = [jobID, job_state, group, job_name, workdir, node_type, booked_time, used_time, point, iteration]
    #    list_for_loop = [jobID, job_state, group, job_name, workdir, node_type, booked_time, used_time, booked_cost, used_cost, iteration]
    elif "-f" in argv:
        list_for_loop = [jobID, job_state, group, job_name, workdir, node_type, booked_time, used_time, point, iteration, maxforce, force_atom]
    elif "-d" in argv:
        list_for_loop = [jobID, job_state, group, job_name, workdir, node_type, iteration, step_dim, force_dim, torque_dim, curvature_dim, angle_dim]

    list_for_loop = [adjust_len(x) for x in list_for_loop]

    L = "\n"; i = 0
    for i1 in range(0,len(list_for_loop[0])): L += " " + " ".join([str(list_for_loop[i2][i1]) for i2 in range(0,len(list_for_loop))]) + "\n"

    return L, list_for_loop

if __name__ == "__main__": 
    L = qsdir()[0]
    print(L)
    path_rec = "/home/4/17D20121/record"
    path_qstatout = path_rec + "/qstat.out"
    with open(path_qstatout, "w") as fq: fq.write(L)

# END: script
