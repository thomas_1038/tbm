#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import sys, os, shutil
import subprocess as sub
import addmol.helper as hl
#from pnnl import mkpot as mp

def mkdf_vib(nowpath, direc_ref):
    #####################
    script = "mkdovasp_tbm.py"
    vasptype = 544
    nodetype = "fn"
    numnode = 1
    #process = 4
    hour = "24:00:00"
    jobname = "vib"
    #ldos_eint_l = -1.0
    #ldos_eint_h = 0.0
    ###############

    needfiles = ["INCAR","KPOINTS","POTCAR","CONTCAR","WAVECAR","CHGCAR"]
    dict_tag ={"LWAVE =":"LWAVE = F\n",
               "LCHARG =":"LCHARG = F\n",
               "ISTART = 0":"ISTART = 1; ICHARG = 1\n",
               "IBRION =":"IBRION = 5\n",
               "POTIM = ":"POTIM = 0.02\n",
               "NFREE = ":"NFREE = 2\n",
               "EDIFF =":"EDIFF = 1e-8\n",
               "ISPIN = ":"ISPIN = 1\n",
               "NSW =":"NSW = 1\n",
               "NELM =":"NELM = 1000\n",
               "NELMIN =":"NELMIN = 6\n"
               }

    # Path
    path_ref = nowpath + "/" + direc_ref
    path_vib = nowpath + "/vib-freq_" + direc_ref
    os.mkdir(path_vib)

    for f in needfiles:
        if not os.path.exists(path_ref+"/"+f):
            if f == "CONTCAR":
                print("CONTCAR does not exist. So POSCAR is copied instead CONTCAR!"); f = "POSCAR"
            else:
                print(f + " does not exist.You should prepare " + f +"!"); continue

        if f == "CONTCAR":
            shutil.copy(path_ref+"/"+f, path_vib+"/POSCAR")

        elif f == "INCAR":
            with open(path_ref+"/"+f) as fi: incarlines = fi.readlines()

            npar = int([hl.conv_line(l)[2] for l in incarlines if "NPAR =" in l][0])
            kpar = int([hl.conv_line(l)[2] for l in incarlines if "KPAR =" in l][0])
            process = npar * kpar

            L = ""
            for line in incarlines:
                for key in dict_tag:
                    #if key == "LVTOT =" and key in line:
                    #    if f_lvtot == "T": L += str(dict_tag[key]); break
                    #elif key == "LAECHG =" and key in line:
                    #    if f_laechg == "T": L += str(dict_tag[key]); break
                    if key in line: L += str(dict_tag[key]); break
                else: L += line
            with open(path_vib+"/INCAR","w") as fi: fi.write(L)

        else: shutil.copy(path_ref+"/"+f, path_vib)
        print(f)

    if os.path.exists(nowpath + "/job.sh"):
        with open(nowpath + "/job.sh") as fj: joblines = fj.readlines()
        jobname = "vib_" + str([l.replace("#$ -N ","").replace("\n","") for l in joblines if "#$ -N" in l][0])
        #print(jobname); sys.exit()

    # Make dovasp
    mkjob= str(script) + " -" + str(vasptype) + " -" + nodetype + " -nn " + str(numnode) + " -np " + str(process) + " -t " + hour + " -n " + jobname

    os.chdir(path_vib)
    if not os.path.exists(path_vib+"/POTCAR"):
        sub.call("mkpot.py", shell=True) #mp.mk_pot()
    sub.call(mkjob, shell=True)
    #with open(path_vib + "/job.sh","a") as fj: fj.write(Ljobadd)

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 1:
        direc_ref = input("Please input the directory name you want to use files in: ")
        #lvtot = input("Do you calculate LOCPOT? [y/N]: ")
        #if lvtot in ["y", "Y"]: f_lvtot = "T"
        #elif lvtot in["n", "N"]: f_lvtot= "N"
        #laechg = input("Do you calculate all electron Bader charge? [y/N]: ")
        #if laechg in["y", "Y"]: f_laechg = "T"
        #elif laechg in["n", "N"]: f_laechg = "N"

    else:
        if str(argv[1]) in ["--h" or "-help"]:
            print("\n--h or -help: show HELP")
            print("--rf: input reference directory")
            #print("--lp: LVTOT = .TRUE.")
            #print("--bd: LAECHG = .TRUE.\n")
            sys.exit()

        if "--rf" in argv:
            index_r = argv.index("--rf"); direc_ref = argv[index_r +1]

        # LVTOT
        #if "--lp" in argv: f_lvtot= "T"
        #else: f_lvtot= "N"

        # LAECHG
        #if "--bd" in argv: f_laechg = "T"
        #else: f_laechg = "N"

    mkdf_vib(os.getcwd(), direc_ref); print("\nDone!\n")
# END: Program