#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys,shutil, threading, glob
import subprocess as sub
import numpy as np

#ad_dirs = ["ontop","bridge","hollow"]
rm_dirs = ["origin","models","tmp","POSCARs","CONTCARs"]
nodes = ["f_node","h_node","q_node","s_gpu"]

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
files = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x)]
dirs = sorted([x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and x not in rm_dirs])

Q_nodetype = input("Please input the type of node, [f_node, fn / h_node, hn / q_node, qn / s_core, sc / q_core, qc / s_gpu, g ]: ")
if Q_nodetype in ["fn", "f_node"]:  node = "f_node"
elif Q_nodetype in["hn", "h_node"]: node = "h_node"
elif Q_nodetype in["qn", "q_node"]: node = "q_node"
elif Q_nodetype in["g", "s_gpu"]:   node = "s_gpu"

num_node = input("Please input the number of node you want to use. e.g., 4; ")
process = input("Please input the number of np: ")
job_time = input("Please input job time, i.e., 3:00:00: ")

for d in dirs:
    print(d)
    path_job = path + "/" + d + "/job.sh"
    with open(path_job) as fj: joblines = fj.readlines()

    L = ""
    for x in joblines:
        for n in nodes:
            if "#$ -l " + n in x: x = "#$ -l " + node + "=" + str(num_node) + "\n"
        if "#$ -l h_rt=" in x: x = "#$ -l h_rt=" + job_time + "\n"
        if "process=" in x: x = "process=" + str(process) + "\n"
        L += x

    with open(path_job, "w") as fj: fj.write(L)

print("Done!")
