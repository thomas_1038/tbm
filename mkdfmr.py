#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2016/05/15 ver. 0.000
#2016/07/11 ver. 0.100
#2016/10/05 ver. 1.000 #adapt the new criterion
#2016/10/15 ver. 1.001

import os, re, sys, shutil, threading
from stat import *
import subprocess as sub
import mkpot

def mk_dfmr(nowpath):
    ############ START: PLEASE INPUT ################
    inputfile = "vauto-input"
    #path_vasp = "/home/4/17D20121/vasp/vasp.5.4.4_neb_190408/bin/"
    path_vasp = "/home/4/17D20121/vasp/vasp.5.4.4_neb_190531/bin/"
    #module = "module load cuda/8.0.44 intel intel-mpi fftw"
    module = "module load cuda/8.0.61 intel/16.0.4.258 intel-mpi/17.3.196 fftw"
    mail_option = "be" #a: 中止、 b: ジョブ実行、e: ジョブ終了
    email = "sakaguchi.t.ad@m.titech.ac.jp"
    #path_qsub = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qsub"
    ############ END: PLEASE INPUT ##################

    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def mkarray(dirs_l,j):
        if j == 0: L =  "array=("
        else: L =  "#array=("
        num = 0
        for dir_prec_l in dirs_l:
            if num == int(len(dirs_l) -1):
                L += " \"" + dir_prec_l + "\")\n"
                break
            L += "\"" + dir_prec_l + "\" "
            num += 1
        return L

    def mk_dovasp(node_type, num_node, job_time, job_name, path_vasp, vasp_type, process, dirs, dirs1, dirs2):
        Lsh =  "#!/bin/sh\n"
        Lsh += "#$-cwd\n"
        Lsh += "#$ -l " + str(node_type) + "=" + str(num_node) + "\n"
        Lsh += "#$ -l h_rt=" + str(job_time) + "\n"
        Lsh += "#$ -N " + str(job_name) + " \n"
        Lsh += "#$ -m " + mail_option + "\n"
        Lsh += "#$ -M " + email + "\n"
        Lsh += ". /etc/profile.d/modules.sh\n"
        Lsh += module + "\n\n"
        Lsh += "path_vasp=\"" + str(path_vasp) + "\"\n"
        Lsh += "vasp_type=\"" + str(vasp_type) + "\"\n"
        Lsh += "process=" + str(process) +  "\n\n"
        Lsh += "echo \"NEW DATE\" >> date.txt\n"
        Lsh += "date >> date.txt\n\n"

        num_d = 0
        for direc_prec in dirs2:
            Lsh += mkarray(dirs,num_d); del dirs[0]; num_d += 1

        Lsh += "for ((i = 0; i < ${#array[@]}; i++))\n"
        Lsh += "\n"
        Lsh += "do\n"
        Lsh += "    cd ./${array[$i]}\n"
        Lsh += "    cp INCAR_act INCAR\n"
        Lsh += "    echo \"NEW DATE\" >> date.txt\n"
        Lsh += "    date >> date.txt\n"
        Lsh += "    mpirun -n $process $path_vasp$vasp_type > std.out\n\n"
        Lsh += "    while read line\n"
        Lsh += "    do\n"
        Lsh += "        if [[ `echo $line|grep 'reached required accuracy'` ]]; then\n"
        Lsh += "            break\n"
        Lsh += "        elif  [[ `echo $line|grep 'BAD TERMINATION OF ONE OF YOUR APPLICATION PROCESSES'` ]]; then\n"
        Lsh += "            date >> date.txt\n"
        Lsh += "            exit 1\n"
        Lsh += "        fi\n"
        Lsh += "    done < ./std.out\n"
        Lsh += "    date >> date.txt\n\n"
        Lsh += "    I=$((${#array[*]}-1))\n"
        Lsh += "    if [ $i = $I ];then\n"
        Lsh += "        echo \"break\"\n"
        Lsh += "        break\n"
        Lsh += "    fi\n\n"
        Lsh += "    j=$(($i+1))\n"
        Lsh += "    cp CONTCAR ../${array[$j]}/POSCAR\n"
        Lsh += "    cp WAVECAR ../${array[$j]}/WAVECAR\n"
        Lsh += "    cp CHGCAR ../${array[$j]}/CHGCAR\n\n"
        Lsh += "    cd ../\n"
        Lsh += "done\n"
        Lsh += "date >> date.txt\n"
        return Lsh

    def mk_dovasp_test(node_type, num_node, job_time, job_name, path_vasp, vasp_type, process, dirs, dirs1, dirs2):
        # make jobtest.sh
        Lsht =  "#!/bin/sh\n"
        Lsht += "#$-cwd\n"
        Lsht += "#$ -l f_node=1\n"
        Lsht += "#$ -l h_rt=0:10:00\n"
        Lsht += "#$ -N " + str(job_name) + "_test\n"
        Lsht += "#$ -m " + mail_option + "\n"
        Lsht += "#$ -M " + email + "\n"
        Lsht += ". /etc/profile.d/modules.sh\n"
        Lsht += module + "\n\n"
        Lsht += "path_vasp=\"" + str(path_vasp) + "\"\n"
        Lsht += "vasp_type=\"vasp_gpu\"\n"
        Lsht += "precess=2\n"

        num_d = 0
        for direc_prec in dirs2:
            Lsht += mkarray(dirs1, num_d); del dirs1[0]; num_d += 1

        Lsht += "for ((i = 0; i < ${#array[@]}; i++))\n"
        Lsht += "do\n"
        Lsht += "    cd ./${array[$i]}\n"
        Lsht += "    cp INCAR_test INCAR\n"
        Lsht += "    echo \"NEW DATE\" >> date.txt\n"
        Lsht += "    date >> date.txt\n"
        Lsht += "    mpirun -n $precess $path_vasp$vasp_type > std.out\n"
        Lsht += "    date >> date.txt\n\n"
        Lsht += "    I=$((${#array[*]}-1))\n"
        Lsht += "    if [ $i = $I ];then\n"
        Lsht += "        echo \"break\"\n"
        Lsht += "        break\n"
        Lsht += "    fi\n\n"
        Lsht += "    j=$(($i+1))\n"
        Lsht += "    cp CONTCAR ../${array[$j]}/POSCAR\n"
        Lsht += "    cp WAVECAR ../${array[$j]}/WAVECAR\n"
        Lsht += "    cp CHGCAR ../${array[$j]}/CHGCAR\n\n"
        Lsht+= "    cd ../\n"
        Lsht += "done\n"
        return Lsht

    def mk_kp(k1, k2, k3, kp_method):
        Lkp = "Automatic mesh\n0\n"
        if kp_method == "G": Lkp += "Gamma\n"
        else: Lkp += "Monkhorst-pack\n"
        Lkp += str(k1) + " " + str(k2) + " " + str(k3) + "\n0 0 0\n"
        return Lkp

    # Definition of path
    path_input = nowpath + "/" + inputfile

    # Read input file
    with open(path_input) as f: inputlines = f.readlines()
    ilist = [conv_line(line) for line in inputlines if len(conv_line(line)) > 0]
    del ilist[0]; del ilist[-1]

    num_t = 0; num_c = 10000; clist = []
    for tag in ilist:
        if "LT:" in tag: LT = float(tag[1])
        elif "calc_set:" in tag: calcset = str(tag[1])
        elif "job_name:" in tag: job_name = str(tag[1])
        elif "kp_method:" in tag: kp_method = str(tag[1])
        elif "cp_wc:" in tag: cp_wc = str(tag[1])
        elif "vasp_type:" in tag: vasp_type = str(tag[1])
        elif "node_type:" in tag: node_type = str(tag[1])
        elif "num_node:" in tag: num_node = str(tag[1])
        elif "process:" in tag: process = str(tag[1])
        elif "NPAR:" in tag: NPAR = str(tag[1])
        elif "KPAR:" in tag: KPAR = str(tag[1])
        elif "NSIM:" in tag: NSIM = str(tag[1])
        elif "job_time:" in tag: job_time = str(tag[1])
        elif "Prec" in tag: num_c = num_t
        if num_t > num_c and tag[0] != "#": clist.append(tag)
        num_t += 1

    path_calcset = nowpath + "/" + calcset
    POSCAR_ini = path_calcset + "/POSCAR"
    POTCAR_ini = path_calcset + "/POTCAR"
    INCAR_ini =  path_calcset + "/INCAR"
    KPOINTS_ini = path_calcset + "/KPOINTS"
    WAVECAR_ini = path_calcset + "/WAVECAR"
    CHGCAR_ini = path_calcset + "/CHGCAR"
    #shfiles = filter(lambda x : ".sh" in x , os.listdir(path_calcset))

    ### Start: make the directories ###
    dirs =[]; dirs1 = []; dirs2 = []
    for num_prec, name in enumerate(clist):
        prec = name[0]
        ecut = name[1]
        k1 = name[2]; k2 = name[3]; k3 = name[4]
        ibrion = name[5]; nelmin = name[6]
        ediff = name[7]; fdiff = name[9]

        if str(name[8]) == "F":
            dirname = str(num_prec) + "_Prec" + str(prec) + "cut" + str(ecut) + "k" + str(k1) + "x" + str(k2) + "x" + str(k3) + "Ed" + str(ediff)
        if str(name[8]) == "T":
            dirname = str(num_prec) + "_Prec" + str(prec) + "cut" + str(ecut) + "k" + str(k1) + "x" + str(k2) + "x" + str(k3) + "Ed" + str(ediff) + "Fd" + str(fdiff)

        print(dirname)
        path_dir = nowpath + "/" + dirname
        os.mkdir(path_dir); dirs.append(dirname); dirs1.append(dirname); dirs2.append(dirname)

        # copy INCAR, KPOINTS and POTCAR in calcset
        #shutil.copy(KPOINTS_ini,path_dir)
        if num_prec == 0: shutil.copy(POSCAR_ini, path_dir)
        #mkpot.mk_pot(path_dir)
        shutil.copy(POTCAR_ini, path_dir)

        # make KPOINTS
        Lkp = mk_kp(k1, k2, k3, kp_method)
        with open(path_dir + "/KPOINTS", "w") as fkp: fkp.write(Lkp)

        # read INCAR
        with open(INCAR_ini) as fin: incarlines = fin.readlines()
        i = 0
        while i < len(incarlines):
            if incarlines[i].find("SYSTEM =") >= 0:
                incarlines[i] = "SYSTEM = " + str(job_name) + ", cutoff:" + str(ecut) + " eV" + " k:" + str(k1) +"x"+ str(k2)+ "x" +  str(k3) + "\n"
            elif incarlines[i].find("PREC =") >= 0 and incarlines[i].find("SYMPREC =") < 0: incarlines[i] = "PREC = " + str(prec) + "\n"
            elif incarlines[i].find("ENCUT =") >= 0: incarlines[i] = "ENCUT = " + str(ecut) + "\n"
            elif incarlines[i].find("IBRION =") >= 0: incarlines[i] = "IBRION = " + str(ibrion) + "\n"
            elif incarlines[i].find("NELMIN =") >= 0: incarlines[i] = "NELMIN = " + str(nelmin) + "\n"
            elif incarlines[i].find("NPAR =") >= 0: incarlines[i] = "NPAR = " + str(NPAR) + "\n"
            elif incarlines[i].find("KPAR =") >= 0: incarlines[i] = "KPAR = " + str(KPAR) + "\n"
            elif incarlines[i].find("NSIM =") >= 0: incarlines[i] = "NSIM = " + str(NSIM) + "\n"
            elif incarlines[i].find("EDIFF =") >= 0: incarlines[i] = "EDIFF = " + str(ediff) + "\n"
            elif incarlines[i].find("EDIFFG =") >= 0:
                if str(name[8]) == "F": incarlines[i] = "!EDIFFG = " + "\n"
                elif str(name[8]) == "T": incarlines[i] =  "EDIFFG = -" + str(fdiff) + "\n"
            elif incarlines[i].find("ICHARG = 2 ; ISTART = 0") >= 0 and num_prec > 0:
                incarlines[i] = "!" + incarlines[i] + "\n"
            elif incarlines[i].find("!ICHARG = 1 ; ISTART = 1") >= 0 and num_prec > 0:
                incarlines[i] = "ISTART = 1 ; ICHARG = 1\n"
            elif incarlines[i].find("ISTART = 0 ; ICHARG = 2") >= 0 and num_prec > 0:
                incarlines[i] = "!" + incarlines[i] + "\n"
            elif incarlines[i].find("!ISTART = 1 ; ICHARG = 1") >= 0 and num_prec > 0:
                incarlines[i] = "ISTART = 1 ; ICHARG = 1\n"
            elif incarlines[i].find("POTIM = ") >= 0 and num_prec > 0:
                incarlines[i] = "POTIM = 0.2\n"

            i += 1

        # make INCAR
        with open(path_dir + "/INCAR_act",'w') as fina: fina.write("".join(incarlines))
        Lt = ""
        for line in incarlines:
            if line.find("NPAR = ") >= 0: line = "NPAR = 2\n"
            elif line.find("KPAR = ") >= 0: line = "KPAR = 1\n"
            Lt += line
        with open(path_dir + "/INCAR_test", "w") as fint: fint.write(Lt)
    # END: make directories #

    # make dovasp
    Lsh = mk_dovasp(node_type, num_node, job_time, job_name, path_vasp, vasp_type, process, dirs, dirs1, dirs2)
    path_job = nowpath + "/job.sh"
    with open(path_job, "w") as fsh: fsh.write(Lsh)
    os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

    Lsht = mk_dovasp_test(node_type, num_node, job_time, job_name, path_vasp, vasp_type, process, dirs, dirs1, dirs2)
    path_testjob = nowpath + "/testjob.sh"
    with open(path_testjob, "w") as fsh: fsh.write(Lsht)
    os.chmod(path_testjob, S_IRUSR | S_IWUSR | S_IXUSR)

if __name__ == "__main__":
    mk_dfmr(os.getcwd()); print("Done!\n")
# END: program
