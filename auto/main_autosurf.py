#!/usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os, sys, shutil
import subprocess as sub
import numpy as np
import addmol.readpos as rp
import addmol.helper as hl
import make.mkkplist as kpl
import calccutkp as cc
import calcinbulk as cb
import calcslab as cs
Cb = cb.Calc_bulk()

def make_bulkdir(nowpath):
    ciffiles = sorted([x for x in os.listdir(nowpath) if ".cif" in x])
    path_cifs = nowpath + "/cifs"
    if not os.path.exists(path_cifs): os.mkdir(path_cifs)
    for cif in ciffiles:
        with open(nowpath + "/" + cif) as fc: ciflines = fc.readlines()
        dirname = ["".join(hl.conv_line(x.replace("_chemical_formula_structural","").replace("'",""))) for x in ciflines if "_chemical_formula_structural" in x][0]

        print(dirname)
        path_dir = nowpath + "/" + dirname
        path_bulk = path_dir + "/bulk"
        path_cutkp1p = path_bulk + "/cutkp1p"
        if not os.path.exists(path_dir): os.mkdir(path_dir)
        if not os.path.exists(path_bulk): os.mkdir(path_bulk)
        if not os.path.exists(path_cutkp1p): os.mkdir(path_cutkp1p)
        shutil.copy(nowpath + "/" + cif, path_cutkp1p)
        shutil.move(nowpath + "/" + cif, path_cifs)
        cc.calc_cutkp(path_cutkp1p, "-fn")

def make_slabdir_from_bulkinfo(nowpath):
    flag_req = "reached required accuracy - stopping structural energy minimisation"
    path_bulk = nowpath + "/bulk"
    path_relax = path_bulk + "/" + [dr for dr in os.listdir(path_bulk) if os.path.isdir(path_bulk + "/" + dr) and "relax_cut" in dr][0]
    path_slab = nowpath + "/slab"
    path_slabonly = path_slab + "/only"
    if not os.path.exists(path_slab): os.mkdir(path_slab)
    if not os.path.exists(path_slabonly): os.mkdir(path_slabonly)

    # get the best kp density
    kp_dense = Cb.get_bestkp(path_bulk)[1]

    # get total energy and the number of atoms from relax
    path_pos_in_relax = path_relax + "/POSCAR"
    with open(path_pos_in_relax) as fp: poslines = fp.readlines()
    sum_atoms = rp.get_sumofnumofel(poslines)

    path_std = path_relax + "/std.out"
    with open(path_std) as fs: stdlines = fs.readlines()
    with open(path_std) as fs: stdline = fs.read()
    if not flag_req in stdline: print(stdline); print("The relaxation does not converge! BYE!"); sys.exit()

    Etot = [float(hl.conv_line(x)[4]) for x in stdlines if "F=" in x][-1]
    Etot_atom = Etot/sum_atoms
    Ls = "Etot: " + str(Etot) + "\n"
    Ls += "N: " + str(sum_atoms) + "\n"
    Ls += "Etot_atom: " + str(Etot_atom)
    with open(path_slabonly + "/energy_bulk.dat", "w") as fso: fso.write(Ls)   

    # copy files from relax to slabonly
    #path_cont = path_relax + "/CONTCAR"
    #shutil.copy(path_cont, path_slabonly + "/POSCAR")
    
    #cs.calc_slab_dirs(path_slabonly, "-mk", kp_dense)
    #cs.calc_slab_dirs(path_slabonly, "-q")

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) > 1 and argv[1] == "-mb": make_bulkdir(os.getcwd())
    elif len(argv) > 1 and argv[1] == "-ms": make_slabdir_from_bulkinfo(os.getcwd())
    print("Done!")
