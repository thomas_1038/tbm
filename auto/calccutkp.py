#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script

import os, sys, shutil, mkpot, re
import subprocess
import make.mkkplist as mkkplist
import chgincarnks as cin
import mkdovasp_tbm as mkdv

def calc_cutkp(nowpath, ntype="-fn", nn=1, np=4, npar=4, kpar=1, nsym=24, list_cutoff=[500], time="00:10:00", flag_pc="c"):
    email = "sakaguchi.t.ad@m.titech.ac.jp"
    group = "tga-MCES01"

    if ntype == "-fn":  node = "f_node"
    elif ntype == "-qn": node = "q_node"
    elif ntype == "-hn": node = "h_node"
    elif ntype == "-g":  node = "s_gpu"

    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0:line.remove("")
        return line

    path_cifs = nowpath + "/cifs"
    if not os.path.exists(path_cifs): os.mkdir(path_cifs)

    ciffiles = sorted([x for x in os.listdir(nowpath) if ".cif" in x])
    for cif in ciffiles:
        path_cif = nowpath + "/" + cif

        #if not os.path.exists(path_cif_in_dir): print("You should prepare the cif-file! BYE!")
        with open(path_cif) as fc: ciflines = fc.readlines()
        name = ["".join(conv_line(x.replace("_chemical_formula_structural","").replace("'","").replace("(","").replace(")",""))) for x in ciflines if "_chemical_formula_structural" in x][0]
        #chemical_formula = [[int(re.sub(r'[^0-9]',"",x)) for x in conv_line(x.replace("_chemical_formula_sum ","").replace("'",""))] for x in ciflines if "_chemical_formula_sum" in x]
        list_str_type = ["".join(conv_line(x.replace("_chemical_name_structure_type","").replace("'","").replace("(","").replace(")",""))) for x in ciflines if "_chemical_name_structure_type" in x]

        if len(list_str_type) > 0:
            path_calc_dir = nowpath  +"/" + name + "_" + list_str_type[0]
        else:
            path_calc_dir = nowpath + "/" + name
        #path_calc_dir = nowpath + "/" + cif.replace("_conventional_standard","").replace(".cif","")
        if not os.path.exists(path_calc_dir): os.mkdir(path_calc_dir)

        path_bulk = path_calc_dir + "/bulk"
        if not os.path.exists(path_bulk): os.mkdir(path_bulk)

        path_cutkp1p = path_bulk + "/cutkp1p"
        if not os.path.exists(path_cutkp1p): os.mkdir(path_cutkp1p)

        shutil.copy(path_cif, path_cutkp1p)
        path_cif_in_dir = path_cutkp1p + "/" + cif

        if not os.path.exists(path_cif_in_dir): print("You should prepare the cif-file! BYE!")
        with open(path_cif_in_dir) as fc: ciflines = fc.readlines()
        jobname = ["".join(conv_line(x.replace("_chemical_formula_structural","").replace("'","").replace("(","").replace(")",""))) for x in ciflines if "_chemical_formula_structural" in x][0]
        #chemical_formula = [[int(re.sub(r'[^0-9]',"",x)) for x in conv_line(x.replace("_chemical_formula_sum ","").replace("'",""))] for x in ciflines if "_chemical_formula_sum" in x]

        if flag_pc == "c":
            cif2pos = 'cif2cell -p vasp --vasp-format=5 --vasp-encutfac=1.0 --vasp-pseudo-priority=\"_d,_pv,_sv,_h,_s\" --vasp-print-species ' +\
                    '--vasp-cartesian-lattice-vectors -f '+ path_cif_in_dir + ' --no-reduce'
        else:
            cif2pos = 'cif2cell -p vasp --vasp-format=5 --vasp-encutfac=1.0 --vasp-pseudo-priority=\"_d,_pv,_sv,_h,_s\" --vasp-print-species ' +\
                    '--vasp-cartesian-lattice-vectors -f '+ path_cif_in_dir

        os.chdir(path_cutkp1p)
        subprocess.call(cif2pos, shell=True)

        path_calc = path_cutkp1p + "/calc"
        if not os.path.exists(path_calc): os.mkdir(path_calc)

        # copy POSCAR in calc
        shutil.copy(path_cutkp1p + "/POSCAR", path_calc)
        # make kplist
        kplist = mkkplist.mk_kplist(path_calc, "F")[1]
        # make POTCAR in calc
        mkpot.mk_pot(path_calc)
        # Convert INCAR for NPAR and KPAR
        os.chdir(path_calc)
        subprocess.call("mkincar.py -ck1m", shell=True)
        cin.conv_INCAR(path_calc, "INCAR", npar, kpar, nsym)

        # Definition
        path_pos_bf = path_calc + "/POSCAR"
        path_pot_bf = path_calc + "/POTCAR"
        path_inc_bf = path_calc + "/INCAR"

        if not os.path.exists(path_pos_bf) or not os.path.exists(path_pot_bf) or not os.path.exists(path_inc_bf):
            print("\nYou should prepare POSCAR, POTCAR or INCAR in calc! BYE!\n")
            sys.exit()

        for num_c, encut in enumerate(list_cutoff):
            for num_k, l in enumerate(kplist):
                kp = l[0]
                direcname = "cut" + str(encut) + "k" + str(kp[0]) + "x" + str(kp[1]) + "x" + str(kp[2])
                path_dir = path_cutkp1p + "/" + str(direcname)
                os.mkdir(str(path_dir))

                #difinition
                path_pos_af = path_dir + "/POSCAR"
                path_pot_af = path_dir + "/POTCAR"
                path_inc_af = path_dir + "/INCAR"

                #copy POSCAR, POTCAR, INCAR
                shutil.copy(path_pos_bf, path_pos_af)
                shutil.copy(path_pot_bf, path_pot_af)

                #make KPOINTS
                os.chdir(path_dir)
                subprocess.call("mkkp.py " + str(kp[0]) + " " + str(kp[1]) + " " + str(kp[2]) + " m", shell=True)

                #make .sh
                job_name = str(jobname) + "-c" + str(encut) + "k" + str(kp[0]) + str(kp[1]) + str(kp[2])

                L = mkdv.make_dovasp(node, "vasp_gpu", nn, job_name, np, time)
                L = L.replace("#$ -m be\n","").replace("#$ -M " + email + "\n","")
                path_job = path_dir + "/job.sh"
                with open(path_job, "w") as fj: fj.write(L)
                os.chmod(path_job, 0o755)

                #make INCAR
                with open(path_inc_bf) as fi: incarlines = fi.readlines()

                num = 0
                while num < len(incarlines):
                    if incarlines[num].find("ENCUT =") >= 0: incarlines[num] = "ENCUT = " + str(encut) + "\n"
                    num += 1
                with open(path_inc_af,'w') as fi: fi.write("".join(incarlines))

                if time == "00:10:00" or time == "0:10:00": subprocess.call("qsub job.sh", shell=True)
                else: subprocess.call("qsub -g " + group + " job.sh", shell=True)

        shutil.move(path_cif, path_cifs)

if __name__ == "__main__":
    argv = sys.argv

    if len(argv) > 1:
        if str(argv[1]) == "-h":
            print("\n")
            print("-h: help")
            print("-nn: the number of node")
            print("-np: the number of thread")
            print("-t: the time for calculation")
            print("\n")
            sys.exit()

        if "-nn" in argv: index_nn = argv.index("-nn"); nn = str(argv[index_nn +1])
        else: nn = input("Please input the number of nodes, e.g., 1: ")

        if "-np" in argv: index_np = argv.index("-np"); np = str(argv[index_np +1])
        else: np = input("Please input the number of np, e.g., 4: ")

        if "-t" in argv: index_t = argv.index("-t"); time = str(argv[index_t +1])
        else: time = input("Please input time of calc, e.g., XX:YY:ZZ: ")

        if "-fn" in argv:   ntype = "-fn"
        elif "-hn" in argv: ntype = "-hn"
        elif "-qn" in argv: ntype = "-qn"
        elif "-g" in argv:  ntype = "-g"
        else: ntype = input("Please input the node type, [-fn, -hn, -qn, -g]: ")

        if "-npar" in argv:
            npar = int(argv[argv.index("-npar") +1])
        else: npar = input("Please input NPAR, e.g., 4: ")

        if "-kpar" in argv:
            kpar = int(argv[argv.index("-kpar") +1])
        else: kpar = input("Please input KPAR, e.g., 1: ")

        if "-nsym" in argv:
            nsym = int(argv[argv.index("-nsym") +1])
        else: nsym = input("Please input NSYM, e.g., 24: ")

    elif len(argv) == 1:
        ntype = input("Please input the node type, [-fn, -hn, -qn, -g] or d (default: -fn): ")
        if ntype in ["d", "defalut"]: ntype = "-fn"

        nn = input("Please input the number of nodes, e.g., 1 or d (default: 1): ")
        if nn in ["d", "defalut"]: nn = "1"

        np = input("Please input the number of np, e.g., 4 or d (default: 4): ")
        if np in ["d", "defalut"]: np = "4"

        q_cutoff = input("Please input the cutoff energies, e.g., 400 450 500 or d (default: 500): ")
        if q_cutoff in ["d", "defalut"]: list_cutoff = [500]
        else: list_cutoff = [int(x) for x in q_cutoff.split(" ")]

        time = input("Please input time of calc, e.g., XX:YY:ZZ or d (default: 00:10:00): ")
        if time in ["d", "defalut"]: time = "00:10:00"

        npar = input("Please input NPAR, e.g., 2 or d (default: 4): ")
        if npar in ["d", "defalut"]: npar = 4
        else: npar = int(npar)

        kpar = input("Please input KPAR, e.g., 2 or d (default: 1): ")
        if kpar in ["d", "defalut"]: kpar = 1
        else: kpar = int(kpar)

        nsym = input("Please input NSYM, e.g., 20 or d (default: 24): ")
        if nsym in ["d", "defalut"]: nsym = 24
        else: nsym = int(nsym)

        flag_pc = input("Please input the primitive cell or conventional cell mode [p/c] or d (default: c): ")
        if flag_pc in ["d", "defalut"]: flag_pc = "c"
        else: flag_pc = flag_pc

    calc_cutkp(os.getcwd(), ntype, nn, np, npar, kpar, nsym, list_cutoff, time)
# END
