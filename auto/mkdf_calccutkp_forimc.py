#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, glob, shutil, math
import calccutkp as ccck

def mk_df_calccutkp(nowpath):
    dirs_GrTM = sorted([x for x in os.listdir(nowpath) if os.path.isdir(nowpath + "/" + x)])

    # 2-Ru, etc.
    for dir_grtm in dirs_GrTM:
        path_dir_grtm = nowpath + "/" + dir_grtm
        dirs_MTM = sorted([x for x in os.listdir(path_dir_grtm) if os.path.isdir(path_dir_grtm + "/" +x )])

        # Mg-Ru, etc
        for dir_mtm in dirs_MTM:
            print("\n" + dir_mtm)
            path_dir_mtm = path_dir_grtm + "/" + dir_mtm
            ciffiles = [x for x in os.listdir(path_dir_mtm) if ".cif" in x]

            for cif in ciffiles:
                dirname = cif.replace(".cif", "")
                print(dirname)

                path_cif = path_dir_mtm + "/" + cif
                path_dir_structure = path_dir_mtm + "/" + dirname
                if not os.path.exists(path_dir_structure): os.mkdir(path_dir_structure)

                path_bulk = path_dir_structure + "/bulk"
                path_cutkp1p = path_bulk + "/cutkp1p"
                path_slab = path_dir_structure + "/slab"
                if not os.path.exists(path_bulk): os.mkdir(path_bulk)
                if not os.path.exists(path_cutkp1p): os.mkdir(path_cutkp1p)
                if not os.path.exists(path_slab): os.mkdir(path_slab)

                shutil.copy(path_cif, path_cutkp1p)
                ccck.calc_cutkp(path_cutkp1p)

if __name__ == "__main__":
    mk_df_calccutkp(os.getcwd())
