#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, glob, shutil, math
import subprocess as sub
import mkpot, mkincar, mkdfmr
import make.mkkplist as mkkp
import addmol.helper as hl
import others.f2cc2f as f2c
import get.getmaxforce as gm
import addmol.readpos as rp

def calc_slab(path_origin, mode, cutoff):
    tsubo_surf ="tsubo.pl --unique_nonpolar 3 3 3 4 < POSCAR > tempfile1 && tsubo.pl --termination_polarity_list tempfile1 < POSCAR > tempfile2 &&  egrep 'A|B' tempfile2 | head -10 > tempfile3 && tsubo.pl --slab_poscar_list tempfile3 20 20 <POSCAR"
    group = "tga-MCES01"
    qsub = "qsub -g " + group + " job.sh"
    kp_ini="o p q"
    cutoff_ini = "XXX"
    dirs_exp = ["tmp", "test", "miss"]

    """
    L_vauto = ""
    L_vauto += "############ START: PLEASE INPUT ##############\n\n"
    L_vauto += "calc_set: calc\n"
    L_vauto += "job_name: job\n"
    L_vauto += "kp_method: M\n"
    L_vauto += "cp_wc: F\n\n"
    L_vauto += "vasp_type: vasp541_std\n"
    L_vauto += "num_node: accel\n\n"
    L_vauto += "Prec Ecut kpoints IBRION NELMIN EDIFF EDIFFG\n"
    L_vauto += "M    500  1 1 1   2 	 2      1e-4  F  No\n"
    L_vauto += "N    500  1 1 1   2 	 4      1e-5  T  1.0e-2\n"
    L_vauto += "A    500  1 1 1   2 	 6      1e-5  T  1.0e-2\n\n"
    L_vauto += "############ END: PLEASE INPUT ################"
    """

    L_vauto = "############ START: PLEASE INPUT ##############\n\n"
    L_vauto += "calc_set: calc\n"
    L_vauto += "job_name: job\n"
    L_vauto += "kp_method: M\n"
    L_vauto += "cp_wc: F\n\n"
    L_vauto += "vasp_type: vasp_gpu\n"
    L_vauto += "node_type: f_node\n"
    L_vauto += "num_node: 1\n"
    L_vauto += "process: 4\n"
    L_vauto += "NPAR: 4\n"
    L_vauto += "KPAR: 1\n"
    L_vauto += "NSIM: 24\n"
    L_vauto += "job_time: 24:00:00\n\n"
    L_vauto += "Prec Ecut kpoints IBRION NELMIN EDIFF EDIFFG\n"
    L_vauto += "M    XXX  o p q   2 	 2      1.0e-4  F  No\n"
    L_vauto += "N    XXX  o p q   2 	 4      1.0e-5  T  3.0e-2\n"
    L_vauto += "A    XXX  o p q   2 	 5      1.0e-5  T  1.0e-2\n"
    L_vauto += "############ END: PLEASE INPUT ################"

    # make directories
    path_bulk = path_origin + "/bulk"
    path_slab = path_origin + "/slab"
    if not os.path.exists(path_slab): os.mkdir(path_slab)
    path_slab_only = path_slab + "/only"
    if not os.path.exists(path_slab_only): os.mkdir(path_slab_only)

    if mode == "mk":
        # make tmp and tmp/vauto-input
        path_tmp = path_slab_only + "/tmp"
        if not os.path.exists(path_tmp): os.mkdir(path_tmp)
        path_vauto = path_tmp + "/vauto-input"
        if not os.path.exists(path_vauto):
            with open(path_vauto, "w") as fv: fv.write(L_vauto)

        # make tmp/calc and tmp/calc/INCAR
        path_calc_in_tmp = path_tmp + "/calc"
        if not os.path.exists(path_calc_in_tmp): os.mkdir(path_calc_in_tmp)
        mkincar.make_incar(path_calc_in_tmp, "s", "For surface calc.", int(cutoff), "F", "N", "1.0e-5", "1", "r", "m", "s")

        # copy CONTCAR from bulk/relax_XXX to slab/only
        path_bulk_relax = path_bulk + "/" + [str(x) for x in os.listdir(path_bulk) if "relax" in x and os.path.isdir(path_bulk+"/"+x)][0]
        path_cont = path_bulk_relax + "/CONTCAR"
        path_pos = path_bulk_relax + "/POSCAR"
        path_pos_in_slabonly = path_slab_only + "/POSCAR"

        with open(path_cont) as fc: contlines = fc.readlines()
        if not contlines:
            shutil.copy(path_pos, path_pos_in_slabonly)
        else:
            shutil.copy(path_cont, path_pos_in_slabonly)
            sumatoms = int(rp.get_sumofnumofel(contlines))

        energies = gm.get_maxforce(path_bulk_relax)
        Etot = float(energies[1][-1])

        Etot_per_atom = Etot/sumatoms
        L =  "Etot_per_atom[eV/atom] Etot[eV] N[atom]\n"
        L += str('{:.3f}'.format(Etot_per_atom)) + " " + str('{:.3f}'.format(Etot)) + " " + str(sumatoms)
        with open(path_slab_only + "/Ebulk.dat","w") as feb: feb.write(L)

        # get kpoints
        path_kp = path_bulk_relax + "/KPOINTS"
        with open(path_kp) as fk: kplines = fk.readlines()
        kp = [int(x) for x in hl.conv_line(kplines[3])]
        kplist = mkkp.mk_kplist(path_slab_only)[1]

        for l in kplist:
            if kp in l: lk_limit = max([int(x) for x in l[1]]); break
            
        os.chdir(path_slab_only)
        sub.call(tsubo_surf, shell=True)

        slabdirs = sorted([dr for dr in os.listdir(path_slab_only) if os.path.isdir(path_slab_only + "/" + dr) and dr not in dirs_exp and "_" in dr])

        #path_tmp = path_slab + "/tmp"
        if not os.path.exists(path_tmp): print("You should prepare tmp! BYE!"); sys.exit()
        for slabdir in slabdirs:
            print(slabdir)
            path_slabdir = path_slab_only + "/" + slabdir
            poscars = [vf for vf in os.listdir(path_slabdir) if "POSCAR" in vf]

            path_models = path_slabdir + "/models"
            if not os.path.exists(path_models): os.mkdir(path_models)
            for i, pos in enumerate(poscars):
                shutil.move(path_slabdir + "/" + pos, path_models + "/s" + slabdir + "_m" + str(i+1) + ".vasp")
            shutil.copytree(path_tmp, path_slabdir + "/tmp")

            poss_in_dir = sorted([x for x in os.listdir(path_models)])
            for pos in poss_in_dir:
                name = pos.replace(".vasp", "")
                print("\n" + name)
                path_pos = path_models + "/" + pos
                path_dir = path_slabdir + "/" + name
                path_calc = path_dir + "/calc"
                path_vinput = path_dir + "/vauto-input"

                # copy tmp as model-name, and copy POSCAR in models to directory as named model-name.
                shutil.copytree(path_tmp, path_dir)
                shutil.copy(path_pos, path_calc)

                # make POSCAR
                Lpos = f2c.f2cc2f(path_calc, pos, "n")
                with open(path_calc + "/POSCAR", "w") as fp: fp.write(Lpos)
                kplist = mkkp.mk_kplist(path_calc, "T")[1]
                mkpot.mk_pot(path_calc)
                for l in kplist:
                    kp = l[0]; lk = l[1]
                    if lk[0] >= lk_limit and lk[1] >= lk_limit:
                        break

                kp_low_str = " ".join([str(math.ceil(kp[0]/2)), str(math.ceil(kp[1]/2)), "1"])
                kp_str = " ".join([str(x) for x in kp])

                # rewrite vauto-input
                with open(path_vinput) as fv: vautolines = fv.readlines()
                Lv = ""; num_t = 0; num_c = 10000
                for line in vautolines:
                    if "job_name:" in line: line = "job_name: " + name + "\n"
                    elif "Prec" in line: num_c = num_t
                    elif num_t > num_c and "M" in hl.conv_line(line):
                        line = line.replace(cutoff_ini + "  " + kp_ini, str(cutoff) + "  " + kp_low_str)
                    elif num_t > num_c and ("N" in hl.conv_line(line) or "A" in hl.conv_line(line)):
                        line = line.replace(cutoff_ini + "  " + kp_ini, str(cutoff) + "  " + kp_str)
                    Lv += line; num_t += 1
                with open(path_vinput, "w") as fv: fv.write(Lv)
                # print(name + " has been made!")
                mkdfmr.mk_dfmr(path_dir)

    if mode == "q":
        slabdirs = sorted([dr for dr in os.listdir(path_slab_only) if os.path.isdir(path_slab_only + "/" + dr) and dr not in dirs_exp and "_" in dr])
        for slabdir in slabdirs:
            path_slabdir = path_slab_only + "/" + slabdir
            calcdirs = sorted([sdr for sdr in os.listdir(path_slabdir) if os.path.isdir(path_slabdir + "/" + sdr) and sdr not in dirs_exp and "_m" in sdr])
            for calcdir in calcdirs:
                path_dir = path_slabdir + "/" + calcdir
                os.chdir(path_dir)
                sub.call(qsub, shell=True)

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) < 2:
        mode = input("Please input the mode; mk or q: ")
        cutoff = input ("Please input the cutoff energy you want use: ")
        calc_slab(os.getcwd(), mode, cutoff)

    elif len(argv) > 1 and str(argv[1]) in ["-h", "--help"]:
        print("\n-h, --help: show HELP")
        print("-mk: make the directories for the surface calculation.")
        print("-q: submit the jobs for the surface calculation\n")
        sys.exit()

    elif len(argv) > 1 and argv[1] == "-mk":
        cutoff = input ("Please input the cutoff energy you want use: ")
        calc_slab(os.getcwd(), "mk", cutoff)
    elif len(argv) > 1 and argv[1] == "-q":
        calc_slab(os.getcwd(), "q", "")
    else:
        print("\nPlease input the command of this shell after script name. if you input -h, you can see the kind of commands. BYE!\n")
        sys.exit()

# END
