#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, glob
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

##################### INPUT ZONE #####################

nodes = ["f_node","h_node","q_node","s_gpu"]

##################### INPUT ZONE #####################

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
    while line.count("") > 0:line.remove("")
    return line

path = os.getcwd()

#booked_cost = []
#booked_cost_ap = booked_cost.append
def calccost():
#    path_dir = path + "/" + x
    path_job = path + "/job.sh"
    with open(path_job) as fj: joblines = fj.readlines()

    for line in joblines:
        if line.find("#$ -l") >= 0:
            for node in nodes:
                if line.find(node) >= 0:
                    numnode = float(line.split("=")[1].replace("\n",""))
                    nodetype = node
                    break

        if line.find("h_rt=") >= 0: btime = conv_line(line)[2].replace("h_rt=","")

    if nodetype == "f_node": point_eff = 1.0
    elif nodetype == "h_node": point_eff = 0.5
    elif nodetype == "q_node": point_eff = 0.25
    elif nodetype == "s_gpu": point_eff = 0.2

    btime_list = btime.split(":")
    btime_sec = 3600*int(btime_list[0]) + 60*int(btime_list[1]) + int(btime_list[2])
    bpoint = round(numnode*point_eff*( 0.7 * float(btime_sec) + 0.1 * float(btime_sec) ),0)
    return [bpoint,nodetype,numnode,btime]

bcost_p = int(calccost()[0])
bcost_yen = int(round(float(bcost_p)/144,0))

print("\nnode_type: " + str(calccost()[1]))
print("num_node: " + str(calccost()[2]))
print("job_time: " + str(calccost()[3]))
print("This job costs [ " + str(bcost_p) + " p. / " +  str(bcost_yen) + " yen ].\n")
# END