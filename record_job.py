#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re, time
import datetime
import subprocess as sub
sys.path.append("/home/4/17D20121/script/tn_scripts_local/tbm")
import qsdir as q

sleep_time = 90
path_record = "/home/4/17D20121/record/"

def record_job():
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def adjust_len(list_qd):
        list_new = []; list_new_ap = list_new.append
        max_len = max([len(x) for x in list_qd])
        L = ""
        for i in range(0,max_len): L += "-"

        for l in list_qd:
            str_num = max_len - len(l); n = 1
            while n < str_num +1: l += " "; n += 1
            list_new_ap(l)

        list_new.insert(1,L)
        return list_new

    # read previous job
    path = os.getcwd()
    list_qsd = q.qsdir()[1]
    if len(list_qsd[0]) == 2: return

    list_qsd = [list_qsd[i] for i in range(0,len(list_qsd)) if i != 1 and i != 6]
    list_title = [x[0].replace(" ","") for x in list_qsd]; list_title.append("state")
    for l in list_qsd: del l[:2]
    list_nowjob_ID = [str(x) for x in list_qsd[0]]

    today = datetime.date.today()
    yesterday =  datetime.date.today() - datetime.timedelta(1)
    path_datefile_today = path_record + str(today).replace("-","") + ".txt"
    path_datefile_yesterday = path_record + str(yesterday).replace("-","") + ".txt"

    if os.path.exists(path_datefile_today) == False and os.path.exists(path_datefile_yesterday) == False:
        path_datefile = path_datefile_today
        list_dy = ["yet" for x in list_nowjob_ID]
        list_qsd.append(list_dy)
        i= 0; list_for_out = []
        for x in list_qsd: x.insert(0, list_title[i]); list_for_out.append(adjust_len(x)); i += 1

    else:
        if os.path.exists(path_datefile_today):
            path_datefile = path_datefile_today
            with open(path_datefile) as fd: dflines = fd.readlines(); del dflines[:2]
            list_prejob_bf = [conv_line(x) for x in dflines]
        else:
            path_datefile = path_datefile_yesterday
            with open(path_datefile) as fd: dflines = fd.readlines(); del dflines[:2]
            list_prejob_bf = [conv_line(x) for x in dflines if "done" not in x]

        list_prejob =  [[list_prejob_bf[i1][i2] for i1 in range(0,len(list_prejob_bf))] for i2 in range(0,len(list_prejob_bf[0]))]
        list_prejob_ID = [str(conv_line(x)[0]) for x in dflines]

        list_dy = []
        for ID in list_prejob_ID:
            if str(ID) in list_nowjob_ID: list_dy.append("yet")
            elif str(ID) not in list_nowjob_ID: list_dy.append("done")

        """
        Lp = ""
        for line in list_prejob: Lp += " ".join(line) + "\n"
        Lq = ""
        for line in list_qsd: Lq += " ".join(line) + "\n"

        print(len(list_prejob))
        print(len(list_qsd))
        #print(Lp + "\n" + Lq)
        sys.exit()
        """

        for ID in list_nowjob_ID:
            ind = list_nowjob_ID.index(ID)
            if str(ID) in list_prejob_ID:
                #print(str(ID))
                for i1 in range(0,len(list_prejob[0])):
                    if ID == list_prejob[0][i1]:
                        #print(str(ID))
                        for i2 in range(0,len(list_prejob)-1):
                            list_prejob[i2][i1] = list_qsd[i2][ind]
                            #print(list_qsd[i2][ind])
                        break
            else:
                for i in range(0,len(list_prejob)-1): list_prejob[i].append(list_qsd[i][ind])
                else: list_dy.append("yet")

        list_prejob[-1] = list_dy
        i= 0; list_for_out = []
        for x in list_prejob: x.insert(0, list_title[i]); list_for_out.append(adjust_len(x)); i += 1

    L = ""
    for i1 in range(0,len(list_for_out[0])):
        L += " " + " ".join([str(list_for_out[i2][i1]) for i2 in range(0,len(list_for_out))]) + "\n"
    with open(path_datefile_today,"w") as fd: fd.write(L)
#    print(L)

#if __name__ == "__main__": record_job()

while True:
    record_job()
    time.sleep(sleep_time)
# END: script