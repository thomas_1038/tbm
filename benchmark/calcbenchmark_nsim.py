#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2017/12/25 ver0.0 made by TN
#This script makes directories for benchmark calculation and runs calculations under each np, NPAR and KPAR.

import os, sys, shutil, threading, subprocess

############################## INPUT ZONE #####################################

path_vasp = "/home/4/17D20121/vasp/vasp.5.4.4_neb_190408/bin/"
module = "module load cuda/8.0.44 intel intel-mpi fftw"
nsims = [4,8,12,16,20,24,28,32,48,64]

############################## INPUT ZONE #####################################

def mkdovasp(nodetype, nodenum, time, jobname, num_np, path_vasp, vasptype):
        line_job = "#!/bin/sh\n"
        line_job += "#$-cwd\n"
        line_job += "#$ -l " + str(nodetype) + "=" + str(nodenum) + "\n"
        line_job += "#$ -l h_rt=" + str(time) + "\n"
        line_job += "#$ -N " + str(jobname) + "\n"
        line_job += ". /etc/profile.d/modules.sh" + "\n"
        line_job += module + "\n\n"
        line_job += "date >> input.txt" + "\n"
        line_job += "mpirun -n " + str(num_np) + " " + str(path_vasp) + str(vasptype) +  " > std.out" + "\n"
        line_job += "date >> input.txt" + "\n"
        return line_job

def mkinput(nodetype, nodenum, memory, num_np, npar, kpar, nsim):
    line_input = str(nodetype) + "\n"
    line_input += str(nodenum) + "\n" # nodenum
    line_input += str(round(float(memory)*float(nodenum)/float(num_np),1)) + "\n"
    line_input += str(num_np) + "\n"       # np
    line_input += str(npar) + "\n"   # NPAR
    line_input += str(kpar) + "\n"   # KPAR
    line_input += str(nsim) + "\n"   # NSIM
    return line_input

def mklistint(I):
    I = float(I); list_int = []; i = 1
    while i <= I:
        list = []; num = I/i
        if num.is_integer() == True:
            list.append(i)
            list.append(int(num))
            list_int.append(list)
        else: pass
        i += 1
    return list_int

Q_calcset = input("Please input the directroy name of calcset, i.e., calc, or d, d=calc: ")
if Q_calcset in ["d", "default"]: calcset = "calc"
else: calcset = str(Q_calcset)

Q_node = input("Please input node type you want to calclate, [f, h, q, g]: ")
if    Q_node == "f": nodetype = "f_node"; num_cpu = 28; memory = 240
elif  Q_node == "h": nodetype = "h_node"; num_cpu = 14; memory = 120
elif  Q_node == "q": nodetype = "q_node"; num_cpu = 7; memory = 60
elif  Q_node == "g": nodetype = "s_gpu";  num_cpu = 2; memory = 30
else: print("Please input [f, h, q, g] only! BYE!"); sys.exit()

Q_vasptype = input("Please input the vasp type you want to calculate, [std, s / gpu, g]: ")
if Q_vasptype in ["std", "s"]: vasptype = "vasp_std"
elif Q_vasptype in ["gpu", "g"]: vasptype = "vasp_gpu"
else: print("You should input [std, s / gpu, g] only! BYE!"); sys.exit()

Q_nodenum = input("Please input the number of nodes, e.g, 1: "); nodenum = int(Q_nodenum)

Q_time = input("Please input the time you want to calclate; defalt,d (default=0:10:00) or X:XX:XX: ")
if Q_time in ["d", "default"]: time = "0:10:00"
else: time = str(Q_time)

# Parameter in INCAR
Q_np = input("Please input np value: "); num_np = str(Q_np)
Q_npar = input("Please input NPAR value: "); npar = str(Q_npar)
Q_kpar = input("Please input KPAR value: "); kpar = str(Q_kpar)
#Q_nsim = input("Please input NSIM value"); nsim = str(Q_nsim)

path = os.getcwd()
path_calc = path + "/" + str(calcset)
#path_job = path_calc + "/job.sh"
INCAR = path_calc + "/INCAR"
KPOINTS = path_calc + "/KPOINTS"
POTCAR = path_calc + "/POTCAR"
POSCAR = path_calc + "/POSCAR"

with open(INCAR) as fin: incarlines = fin.readlines()
num = 0
while num < len(incarlines):
    if incarlines[num].find("NPAR =") >= 0: incarlines[num] = "NPAR = " + npar + "\n"
    elif incarlines[num].find("KPAR =") >= 0: incarlines[num] = "KPAR = " + kpar + "\n"
#    elif incarlines[num].find("NSIM =") >= 0: incarlines[num] = "NSIM = " + nsim + "\n"
    num += 1
with open(INCAR, "w") as fi: fi.write("".join(incarlines))

if Q_node == "f": direcname = "f_node_nn" + str(nodenum) + "_nsim"
elif Q_node == "h": direcname = "h_node_nn" + str(nodenum) + "_nsim"
elif Q_node == "q": direcname = "q_node_nn" + str(nodenum) + "_nsim"
elif Q_node == "g": direcname = "s_gpu_nn" + str(nodenum) + "_nsim"
path_direc = path + "/" + direcname
if not os.path.exists(path_direc): os.mkdir(path_direc)

for nsim in nsims:
    print("np=" + str(num_np) + " NPAR=" + str(npar) + " KPAR=" + str(kpar) + " NSIM=" + str(nsim))
    direc_nk = "np" + str(num_np) + "n" + str(npar) + "k" + str(kpar) + "s" + str(nsim)
    path_direc_nk = path_direc + "/" + direc_nk
    INCAR_nk = path_direc_nk + "/INCAR"
    job_nk = path_direc_nk + "/job.sh"
    path_input = path_direc_nk + "/input.txt"
    os.mkdir(path_direc_nk)

    # copy files
    shutil.copy(POSCAR, path_direc_nk)
    shutil.copy(POTCAR, path_direc_nk)
    shutil.copy(KPOINTS, path_direc_nk)

    # make INCAR
    nsim_in_incarlines = [x for x in incarlines if "NSIM =" in x][0]
    incarlines[incarlines.index(nsim_in_incarlines)] = "NSIM = " + str(nsim) + "\n"
    with open(INCAR_nk, "w") as fin: fin.write("".join(incarlines))

    # make job.sh in direc_nk
    jobname = direc_nk
    with open(job_nk, "w") as fj: fj.write(mkdovasp(nodetype, nodenum, time, jobname, num_np, path_vasp, vasptype))
    chmod = "chmod u+x " + job_nk
    subprocess.call(chmod, shell=True)

    # make input.txt
    with open(path_input, "w") as finp: finp.write(mkinput(nodetype, nodenum, memory, num_np, npar, kpar, nsim))

    # qsub
    os.chdir(path_direc_nk)
    qsub = "qsub job.sh"
    subprocess.call(qsub, shell=True)

print("Done!")
# END: Program
