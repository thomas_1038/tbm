#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
files = os.listdir(path)
direcs = sorted([f for f in files if os.path.isdir(os.path.join(path, f))])

# Get nodetype
path_input0 = path + "/" + str(direcs[0]) + "/input.txt"
with open(path_input0) as finp0: inputlines0 = finp0.readlines()

list_inp = [conv_line(line)[0] for line in inputlines0]
nodetype = list_inp[0]

if nodetype == "f_node": point_eff = 1
elif nodetype == "h_node": point_eff = 0.5
elif nodetype == "q_node": point_eff = 0.25
elif nodetype == "s_gpu": point_eff = 0.2

# Get prepared jobtime
path_job0 = path + "/" + str(direcs[0]) + "/job.sh"
with open(path_job0) as fj0: joblines = fj0.readlines()

list_job = [conv_line(line) for line in joblines]
list_jobtime = list_job[3][2].replace("h_rt=","")
list_jobtime = list_jobtime.split(":")
jobtime = int(list_jobtime[0])*3600 + int(list_jobtime[1])*60 + int(list_jobtime[2])
print(jobtime)

L_tot = "node_type num_node mem thread np kp ns begin end time(sec) point T/F\n"
for direc in direcs:
    print(direc)
    path_direc = path + "/" + str(direc)

    # read INCAR
    path_INCAR = path_direc + "/INCAR"
    with open(path_INCAR) as fin: incarlines = fin.readlines()

    """
    judge_scf = "F"
    for line in incarlines:
        flag_IBRION = line.find("IBRION = -1")
        flag_NSW = line.find("NSW = 0")
        if flag_IBRION > 0 or flag_NSW > 0: 
            judge_scf = "T"
            print(judge_scf)
    """

    # read std.out
    RRA = "F"; dE = "---"; step = "---"
    path_stdout = path_direc + "/std.out"
    if os.path.exists(path_stdout):
        with open(path_stdout) as fstd: stdlines = fstd.readlines()
        if stdlines[-1].find("1 F=") >= 0:
            RRA = "T"
            dE = "{:.1e}".format(float(conv_line(stdlines[-2])[3]))
            step = int(conv_line(stdlines[-2])[1])
        elif conv_line(stdlines[-1])[0] in ["RMM:","DAV"]:
            RRA = "F"
            dE = "{:.1e}".format(float(conv_line(stdlines[-1])[3]))
            step = int(conv_line(stdlines[-1])[1])

    # read input.txt
    path_input = path_direc + "/input.txt"
    with open(path_input) as finp: inputlines = finp.readlines()

    param0 = [conv_line(line) for line in inputlines]
    num_p = 0; param1 = []; time_tot = []
    if RRA == "T":
        for l in param0:
            if num_p == 7 or num_p == 8:  #this means time
                param1.append(l[3]) #add XX:YY:ZZ
                time = l[3].split(":")
                sec = int(time[0])*3600 + int(time[1])*60 + int(time[2])
                time_tot.append(sec)
            else: param1.append(l[0])
            num_p += 1

        else:
            if len(time_tot) == 2: time_diff = time_tot[1] - time_tot[0]
            else: time_diff = jobtime
            param1.append(time_diff)
            if time_diff > 300: point = round(float(param1[1])*point_eff*( 0.7 * float(time_diff) + 0.1 * float(jobtime) ),0)
            else: point  = round(float(param1[1])*point_eff*( 0.7 * 300.0 + 0.1 * float(jobtime) ),0)
            param1.append(point)
            param1.append(RRA)
            param1.append(dE)
            param1.append(step)

    else:
        for l in param0:
            if num_p < 7: param1.append(l[0])
            else: pass
            num_p += 1

        else:
            param1.append("----") #begin_time
            param1.append("----") #end_time
            param1.append(jobtime) #time(sec)
            param1.append(round(float(param1[1])*point_eff*( 0.7 * float(jobtime) + 0.1 * float(jobtime) ),0)) #point
            param1.append(RRA)
            param1.append(dE)
            param1.append(step)

    #print param1
    L_inp = ""
    for param in param1: L_inp += str(param) + "  "
    L_tot += L_inp + "\n"

path_benchmark = path + "/benchmark_" + str(nodetype) + ".txt"
with open(path_benchmark, "w") as fb: fb.write(L_tot)

print("\nDone!\n")
# END: Program