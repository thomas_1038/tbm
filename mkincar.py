#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#make INCAR file
#2015/02/20 version 1.0
#2015/03/25 version 1.1  #delete RWIGS tag.
#2015/03/30 version 1.11 #!ALGO = Fast
#2015/05/14 version 1.2
#2015/05/15 version 1.3
#2015/07/23 verison 1.31
#2015/08/08 version 1.32
#2015/08/11 version 1.321 #add Ionic Relaxation tag
#2015/08/12 version 1.330 #add Hybrid Function tag
#2015/08/13 version 1.331 #add backup making
#2015/09/29 version 1.332 #add LOPTICS and change EDIFF and EDIFFG
#2016/02/13 version 1.400 #add question surface or metal_bulk
#2016/02/17 version 1.401
#2016/02/22 version 1.402 #add DOS parameter and change SYSTEM-tag
#2016/10/25 version 1.403
#2017/05/23 version 1.500 #add question relax or 1p
#2017/10/26 version 1.600
#2017/10/27 version 1.610
#2017/11/06 version 1.615
#2018/08/04 version 1.700
#This script make INCAR file.
#usage:script

import os, sys, shutil

#################### INITIAL PARAMETER ####################
NELM_re = 120
NELMIN = 6
POTIM = 0.1
NSW = 1000
NPAR = 8
NSIM = 24
KPAR = 1
ENCUT = 500
EDIFF = 1.0e-5
EDIFFG = -1.0e-2
#################### INITIAL PARAMETER ####################

def make_incar(nowpath, norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf):
	VER = "1.800 (2019.02.27 update)"
	argv = sys.argv
	#System
	sy = "!! File System !!" + "\n"
	sy += "!INCAR-version: " + VER + "\n"
	sy += "SYSTEM = " + sysname + " cutoff: " +  str(cutoff) + "eV" + "\n" + "\n"

	#Start parameters
	st = "!! Start Parameters !!" + "\n"
	st += "!ISTART:0:from scratch 1:restart with the same Encut" + "\n"
	st += "!2:restart with the same basis set 3:full restart" + "\n"
	st += "!ICHARG:0:Charge from wavefunction 1:CHGCAR 2:Atomic Charge" +"\n"
	st += "ISTART = 0 ; ICHARG = 2\n"
	st += "!ISTART = 1 ; ICHARG = 1\n"
	st += "!ISTART = 2 ; ICHARG = 0\n"
	st += "!NBANDS:At COHP calculation, you set the total number of valence electron." + "\n"
	st += "!NBANDS = " + "\n" + "\n"

	#File control
	lwct = "!! File Control !!\n" + "LWAVE = T\n" + "LCHARG = T\n"
	lwcf = "!! File Control !!\n" + "LWAVE = F\n" + "LCHARG = F\n"
	ft0 = "!LVTOT = T" + "\n"
	ft0 += "!LVHAR = T" + "\n"
	ft0 += "!LELF = T" + "\n"
	ft0 += "!LOPTICS = T" + "\n"
	ft0 += "LORBIT = 11" + "\n"
	ft0 += "!LAECHG = T" + "\n"
	ft0 += "!ADDGRID =T" + "\n"
	ft0 += "!LSORBIT = T" + "\n" + "\n"

	ft    = lwct+ ft0
	ft_ck = lwcf + ft0

	#algorithm
	al = "!! Algorithm !!\n"
	al += "!ALGO and IALGO determines how the wavefunctions are optimized.\n"
	al += "!IALGO=38,ALGO=N:defolt,Davidson block\n"
	al += "!ALGO=Fast:Davidson is used for the first a few steps, and then VASP switches to RMM-DIIS.\n"
	al += "!IALGO=48,ALGO=VeryFast:RMM-DIIS\n"
	al += "ALGO = " + str(algo) + "\n"
	al += "!IALGO = 48\n\n"

	#accuracy
	a0 = "!! Accuracy !!\n"
	a0 += "ENCUT = " + str(cutoff) + "\n"
	a0 += "!PPEC:High|Normal|Medium|Low\n"
	a0 += "PREC = " + prec + "\n"
	a0 += "!LREAL:.Ture.|.False.|Auto|O\n"
	lreala = "LREAL = A\n\n"
	lrealf = "LREAL = F\n\n"

	a    = a0 + lreala
	a_ck = a0 + lreala

	#mixing parameter
	m = "!! Mixing Parameter !!" + "\n"
	m += "!linear mixing" + "\n"
	m += "!AMIX = 0.2" + "\n"
	m += "!BMIX = 0.0001" + "\n"
	m += "!AMIX_MAG = 0.8" + "\n"
	m += "!BMIX_MAG = 0.0001" + "\n\n"

	#electronic relaxation
	e0 = "!! Electronic Relaxation !!" + "\n"
	e0 += "EDIFF = " + ediff  + "\n"
	e0 += "!ISPIN|1:non spin polarized|2:spin polarized" + "\n"
	e0 += "ISPIN = " + spin + "\n"
	e0 += "!NELM:number of electronic step" +"\n"
	nelm_re = "NELM = " + str(NELM_re) + "\n"
	nelm_1p = "NELM = 1000" + "\n"

	e1 = "!NELMIN|bulk:4|complex surface:8|" + "\n"
	e1 += "NELMIN = " + str(NELMIN) + "\n"
	e1 += "!NELECT:number of electron" + "\n"
	e1 += "!NELECT =" + "\n"
	e1 += "!GGA|PB:Perdew-Becke|PW:Perdew-Wang 86|LM:Langreth-Mehl-Hu|" +"\n"
	e1 += "!91:Perdew-Wang 91|PE:Perdew-Bruke-Ernzehof|RP:revised PBE|" + "\n"
	e1 += "!AM:AM05|PS:PBE revised for solids|" + "\n"
	e1 += "!GGA = RP\n\n"
	e1_b = "!NELMDL = -10\n\n"
	e1_s = "NELMDL = -10\n\n"

	e_re = e0 + nelm_re + e1
	e_1p = e0 + nelm_1p + e1

	#ionic relaxation
	ed_1p = "!! Ionic Relaxation !!\n" + "!EDIFFG = " + str(EDIFFG) + "\n"
	ed_re = "!! Ionic Relaxation !!\n" + "EDIFFG = " + str(EDIFFG) + "\n"
	ed_are = "!! Ionic Relaxation !!\n" + "EDIFFG = -1.0e-3" + "\n"

	i0 = "!IBRION|-1:Ion position fixed|0:MD|1:Quasi-Newton(NFREE,POTIM)|" +"\n"
	i0 += "!2:Conjugate Gradient(POTIM)|3:Damped MD,use dumping factor(SMASS,POTIM)" + "\n"
	i0 += "!5,6:Hessian matrix calculated by finite-difference.6:considers symmetry" + "\n"
	i0 += "!7,8:Hessian matrix calculated analytically.8:consides symmetry" + "\n"
	i_1p =  i0 + "IBRION = -1" + "\n"
	i_re =  i0 + "IBRION = 2" + "\n"
	i_are = i0 + "IBRION = 1" + "\n"

	i1 = "!POTIM = " + str(POTIM) + "\n"
	i1 +="!NFREE:the number of vector s kept in the iterarion history." + "\n"
	i1 += "!complex model:10~20|simple model:the number of dgrees of freedom|" + "\n"
	i1 += "!NFREE = " + "\n"
	i1 += "!SMASS|-3:micro canonical|-2:initial velocity are kept const|"  + "\n"
	i1 += "!-1:continuous increase or decrease the kinetic energy|>=0:canonical|" + "\n"
	i1 +="!SMASS = " + "\n"
	i1 += "!ISIF|0:Ion relax only|2:Cell fixed|3:Cell & Volume relax|" + "\n"
	isif2 = i1 + "ISIF = 2" + "\n"
	isif3 = i1 + "ISIF = 3" + "\n"
	nsw_re = "NSW = " + str(NSW) + "\n"
	nsw_1p = "NSW = 0" + "\n"

	i2 = "!ISYM|0:Break symmetry|1:Keep symmetry(USPP)|2:Keep symmetry(PAW)|" + "\n"
	i2 += "ISYM = 0" + "\n"
	i2 += "!SYMPREC = 1e-16" + "\n" + "\n"

	ib_1p    =  ed_1p  + i_1p  + isif3 + nsw_1p + i2
	ib_re    =  ed_re  + i_re  + isif3 + nsw_re + i2
	ib_re_ck =  ed_1p  + i_re  + isif3 + nsw_re + i2
	ib_are   =  ed_are + i_are + isif3 + nsw_re + i2
	isf_1p    = ed_1p  + i_1p  + isif2 + nsw_1p + i2
	isf_re    = ed_re  + i_re  + isif2 + nsw_re + i2
	isf_re_sf = ed_1p  + i_re  + isif2 + nsw_re + i2

	#DOS related values
	#grep RWIGS
	path_pot = nowpath + "/POTCAR"
	if os.path.exists(path_pot):
		with open(path_pot) as fpot: potlines = fpot.readlines()

		rl = []
		for line in potlines:
			if line.find("RWIGS") >= 0: rl.append(line.split(' '))

		i1 = 0; rwigs = []
		while i1 < len(rl): rwigs.append(rl[i1][16]); i1 += 1

		i2 = 1; L = str(rwigs[0])
		while i2 < len(rwigs): L += " " + str(rwigs[i2]); i2 += 1

	else:
		print("\n#########################################################################")
		print("#                                                             #")
		print("# If you need RWIG values, please make POTCAR before using this script. #")
		print("#           This INCAR file don't include RWIGS values.                 #")
		print("#                                                                       #")
		print("#########################################################################")
		L = ""

	pd = "!! DOS related Values !!" + "\n"
	pd += "!RWIGS = " + L + "\n"
	pd += "!ISMEAR:-1:Fermi-smearing 0:Gaussian smearing N:Methfessel-Paxton order N" +"\n"
	pd += "!-4 :tetrahedron method without Bloch corrections" + "\n"
	pd += "!-5 :tetrahedron method with Bloch corrections"+ "\n"
	pd += "!use -5 for semiconductor/isulators, 1 or 2 for metals." +"\n"
	pd += "!use -1 or 0 or 1 or 2 for Band" + "\n"
	pd += "!EMIN,EMAX,NEDOS:minimum & maximum energy for evaluation of DOS, number of grid points of DOS" + "\n"
	pd += "!EMIN = -(lower KS-eigenvalue - derta)" + "\n"
	pd += "!EMAX = -(higher KS-eigenvalue + derta)" + "\n"
	pd += "!NEDOS = 3001" + "\n"
	sm0 = "ISMEAR = 0" + "\n" + "SIGMA = 0.05" + "\n\n"
	sm1 = "ISMEAR = 1" + "\n" + "SIGMA = 0.05" + "\n\n"

	pdi = pd + sm0
	pdm = pd + sm1

	#parallelisation
	pr = "!! Parallelisation !!" + "\n"
	pr += "!NPAR|something trouble:1|usually:4|Hybrid:16|" + "\n"
	pr += "NPAR = " + str(NPAR) + "\n"
	pr += "KPAR = " + str(KPAR) + "\n"
	pr += "NSIM = " + str(NSIM) + "\n"
	pr += "LPLANE = .True." + "\n" + "\n"

	#local dos
	ld = "!! Local DOS related Values !!" + "\n"
	ld += "!LPARD = .True."+"\n"
	ld += "!NBMOD:0:Total charge density incl. unoccupied bands." + "\n"
	ld += "!-1:Usual total charge density(default)." + "\n"
	ld += "!-2:Partial charge density within energies specified by EINT." + "\n"
	ld += "!-3:The same as -2,but the energy is measured from the Fermi level." + "\n"
	ld += "!NBMOD = -3" + "\n"
	ld += "!Energy Range to be incorporated" + "\n"
	ld +="!EINT = -0.05 0.05" + "\n"
	ld += "!Indexes of bands to be used(see p4v or PROCAR)" + "\n"
	ld += "!IBAND = " + "\n"
	ld += "!Indexes of K points to be used(see p4v or PROCAR)" + "\n"
	ld += "!KPUSE = " + "\n" + "\n"

	#Hybrid Function
	hf = "!! Hybrid Function !!" + "\n"
	hf += "!LHFCALC = T" + "\n"
	hf += "!ALGO = All ; TIME = 0.3"+ "\n"
	hf += "!PRECFOCK:FFT grid in H-F and GW related routines" + "\n"
	hf += "!PRECFOCK = Fast" + "\n"
	hf += "!You select HSE06, HFSCREEN = 0.2" + "\n"
	hf += "!HFSCREEN = 0.2" + "\n"
	hf += "!NKRED:grid reduction factor" + "\n"
	hf += "!NKRED = 2" + "\n"
	hf += "!LASPH = .True." + "\n" + "\n"
	hf += "!Additional Hybrid tags" + "\n"
	hf += "!Amount of exact/DFT exchange and correlation" + "\n"
	hf += "!AEXX = " + "\n"
	hf += "!ALDAC = " + "\n"
	hf += "!AGGAX = " + "\n"
	hf += "!AGGAC = " + "\n"
	hf += "!ENCUTFOCK:FFT grid in HF related routines" + "\n"
	hf += "!ENCUTFOCK = " + "\n"
	hf += "!LMAXFOCK = " + "\n"
	hf += "!LAMXFOCKAE = " + "\n"
	hf += "!LTHOMAS = " + "\n"
	hf += "!grid reduction factor" + "\n"
	hf += "!NKREDX = " + "\n"
	hf += "!NKREDY = " + "\n"
	hf += "!NKREDZ = " + "\n"
	hf += "!EVENONLY = " + "\n"
	hf += "!ODDONLY = "  + "\n"

	else_bulk_1p   = sy + st + ft + al + a + m + e_1p + e1_b + ib_1p  + pdi + pr + ld + hf
	else_surf_1p   = sy + st + ft + al + a + m + e_1p + e1_s + isf_1p + pdi + pr + ld + hf
	metal_bulk_1p  = sy + st + ft + al + a + m + e_1p + e1_b + ib_1p  + pdm + pr + ld + hf
	metal_surf_1p  = sy + st + ft + al + a + m + e_1p + e1_s + isf_1p + pdm + pr + ld + hf

	else_bulk_re   = sy + st + ft + al + a + m + e_re + e1_b + ib_re  + pdi + pr + ld + hf
	else_surf_re   = sy + st + ft + al + a + m + e_re + e1_s + isf_re + pdi + pr + ld + hf
	metal_bulk_re  = sy + st + ft + al + a + m + e_re + e1_b + ib_re  + pdm + pr + ld + hf
	metal_surf_re  = sy + st + ft + al + a + m + e_re + e1_s + isf_re + pdm + pr + ld + hf

	else_bulk_1p_ck   = sy + st + ft_ck + al + a_ck + m + e_1p + e1_b + ib_1p    + pdi + pr + ld + hf
	metal_bulk_1p_ck  = sy + st + ft_ck + al + a_ck + m + e_1p + e1_b + ib_1p    + pdm + pr + ld + hf
	else_bulk_re_ck   = sy + st + ft_ck + al + a_ck + m + e_re + e1_b + ib_re_ck + pdi + pr + ld + hf
	metal_bulk_re_ck  = sy + st + ft_ck + al + a_ck + m + e_re + e1_b + ib_re_ck + pdm + pr + ld + hf
	else_bulk_are     = sy + st + ft_ck + al + a_ck + m + e_re + e1_b + ib_are + pdi + pr + ld + hf

	else_surf_re_sf   = sy + st + ft_ck + al + a + m + e_re + e1_s + isf_re_sf + pdi + pr + ld + hf
	metal_surf_re_sf  = sy + st + ft_ck + al + a + m + e_re + e1_s + isf_re_sf + pdm + pr + ld + hf

	def mkincar(L):
		with open(nowpath+"/INCAR","w") as f: f.write(L)

	if norm_ck_surf == "n" and scf_or_relax == "1" and metal_or_else == "m" and bulk_or_surf == "b":
		mkincar(metal_bulk_1p); L1 = "#  INCAR-type: SCF_Metal_Bulk                     #"
	elif norm_ck_surf == "n" and scf_or_relax == "r" and metal_or_else == "m" and bulk_or_surf == "b":
		mkincar(metal_bulk_re); L1 = "#  INCAR-type: Relax_Metal_Bulk                   #"
	elif norm_ck_surf == "n" and scf_or_relax == "1" and metal_or_else == "m" and bulk_or_surf == "s":
		mkincar(metal_surf_1p); L1 = "#  INCAR-type: SCF_Metal_Surface                  #"
	elif norm_ck_surf == "n" and scf_or_relax == "r" and metal_or_else == "m" and bulk_or_surf == "s":
		mkincar(metal_surf_re); L1 = "#  INCAR-type: Relax_Metal_Surface                #"
	elif norm_ck_surf == "n" and scf_or_relax == "1" and metal_or_else == "e" and bulk_or_surf == "b":
		mkincar(else_bulk_1p); L1 = "#  INCAR-type: SCF_Else_Bulk                      #"
	elif norm_ck_surf == "n" and scf_or_relax == "r" and metal_or_else == "e" and bulk_or_surf == "b":
		mkincar(else_bulk_re); L1 = "#  INCAR-type: Relax_Else_Bulk                    #"
	elif norm_ck_surf == "n" and scf_or_relax == "1" and metal_or_else == "e" and bulk_or_surf == "s":
		mkincar(else_surf_1p); L1 = "#  INCAR-type: SCF_Else_Surface                   #"
	elif norm_ck_surf == "n" and scf_or_relax == "r" and metal_or_else == "e" and bulk_or_surf == "s":
		mkincar(else_surf_re); L1 = "#  INCAR-type: Relax_Else_Surface                 #"
	elif norm_ck_surf == "ck" and scf_or_relax == "1" and metal_or_else == "m":
		mkincar(metal_bulk_1p_ck); L1 = "#  INCAR-type: SCF_Metal_Bulk_cutkp               #"
	elif norm_ck_surf == "ck" and scf_or_relax == "r" and metal_or_else == "m":
		mkincar(metal_bulk_re_ck); L1 = "#  INCAR-type: Relax_Metal_Bulk_cutkp             #"
	elif norm_ck_surf == "ck" and scf_or_relax == "1" and metal_or_else == "e":
		mkincar(else_bulk_1p_ck); L1 = "#  INCAR-type: SCF_Else_Bulk_cutkp                #"
	elif norm_ck_surf == "ck" and scf_or_relax == "r" and metal_or_else == "e":
		mkincar(else_bulk_re_ck); L1 = "#  INCAR-type: Relax_Else_Bulk_cutkp              #"
	elif norm_ck_surf == "s" and scf_or_relax == "r" and metal_or_else == "m":
		mkincar(metal_surf_re_sf); L1 = "#  INCAR-type: Relax_Metal_Surface_surf           #"
	elif norm_ck_surf == "s" and scf_or_relax == "r" and metal_or_else == "e":
		mkincar(else_surf_re_sf); L1 = "#  INCAR-type: Relax_Else_Surface_surf            #"
	if len(argv) > 1:
		if str(argv[1]) == "-are":
			mkincar(else_bulk_are); L1 = "#  INCAR-type: Relax_Else_Bulk_are                #"

	L2 = "#  ENCUT = " + str(cutoff) + " ALGO = " + algo + " PREC = " + prec + " EDIFF = " + str(ediff) + "   #"
	print("\n")
	print("###################################################")
	print("#                                                 #")
	print(L1)
	print(L2)
	print("#                                                 #")
	print("###################################################")
	print("\n")

if __name__ == "__main__":
	argv = sys.argv

	def mk_inds(ck, sn, co, ag, pr, ed, sp, sc, me, bu):
		norm_ck_surf = ck; sysname = sn; cutoff = str(co); algo = ag; prec = pr; ediff = ed; spin = sp; scf_or_relax = sc; metal_or_else = me; bulk_or_surf = bu
		return norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf

	if len(argv) > 1:
		if str(argv[1]) == "-h":
			print("\n")
			print("-h  : show HELP.")
			print("-ck1: SCF   for bulk (For determining cut-kp), ENCUT = " + str(ENCUT) + ", EDIFF = 1e-5, ALGO = F, PREC = N, ISPIN = 1, ISMEAR = 0, ISIF = 3")
			print("-ckr: Relax for bulk (For determining cut-kp), ENCUT = " + str(ENCUT) + ", EDIFF = 1e-5, ALGO = F, PREC = N, ISPIN = 1, ISMEAR = 0, ISIF = 3")
			print("-are: Relax for bulk (For accurate calc.),     ENCUT = 700, EDIFF = 1e-6, ALGO = F, PREC = A, ISPIN = 1, ISMEAR = 0, ISIF = 3")
			print("-sm : Relax for metal surface,                 ENCUT = " + str(ENCUT) + ", EDIFF = 1e-5, ALGO = F, PREC = N, ISPIN = 1, ISMEAR = 1, ISIF = 2")
			print("-se : Relax for else surface,                  ENCUT = " + str(ENCUT) + ", EDIFF = 1e-5, ALGO = F, PREC = N, ISPIN = 1, ISMEAR = 0, ISIF = 2")
			print("\n")
			sys.exit()

		elif str(argv[1]) == "-ck1e":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("ck", "For determinig cut-kp", ENCUT, "F", "N", "1.0e-4", "1", "1", "e", "b")
		elif str(argv[1]) == "-ck1m":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("ck", "For determinig cut-kp", ENCUT, "F", "N", "1.0e-4", "1", "1", "m", "b")
		elif str(argv[1]) == "-ckre":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("ck", "For determinig cut-kp", ENCUT, "F", "N", "1.0e-4", "1", "r", "e", "b")
		elif str(argv[1]) == "-ckrm":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("ck", "For determinig cut-kp", ENCUT, "F", "N", "1.0e-4", "1", "r", "m", "b")
		elif str(argv[1]) == "-are":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("n", "For determinig cut-kp", 700, "F", "A", "1.0e-6", "1", "r", "e", "b")
		elif str(argv[1]) == "-sm":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("s", "For surface calc.", ENCUT, "F", "N", "1.0e-5", "1", "r", "m", "s")
		elif str(argv[1]) == "-se":
			norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf \
			= mk_inds("s", "For surface calc.", ENCUT, "F", "N", "1.0e-5", "1", "r", "e", "s")

	elif len(argv) == 1:
		norm_ck_surf = input("Normal calc. or Determining for cutoff-kpoints or Surface calc.?  [n, ck, s]: ")
		if norm_ck_surf == "n":
			sysname = input("Please input the system name, i.e., NaCl: ")
			cutoff = input("Please input cutoff energy, i.e., 500: ")
			algo = input("Please input ALGO-tag, [N or F]: ")
			prec = input("Please input PREC-tag, [L, M, N, A]: ")
			ediff = input("Please input EDIFF, i.e., 1e-6: ")
			spin = input("Please input ISPIN , [1 or 2]: ")
			scf_or_relax = input("1p calculation or Relax? [1/r]: ")
			metal_or_else = input("INCAR-type Metal or else ? [m/e]: ")
			bulk_or_surf = input("Bulk or Surface ? [b/s]: ")

		elif norm_ck_surf == "ck":
			sysname = "For determinig cut-kp"
			cutoff = str(ENCUT)
			algo = "F"
			prec = "N"
			ediff = "1.0e-4"
			spin = "1"
			scf_or_relax = input("1p calculation or Relax? [1/r]: ")
			metal_or_else = input("INCAR-type Metal or else ? [m/e]: ")
			bulk_or_surf = "b"

		elif norm_ck_surf == "s":
			sysname = "For surface calc."
			cutoff = "500"
			algo = "N"
			prec = "N"
			ediff = "1.0e-5"
			spin = "1"
			scf_or_relax = "r"
			metal_or_else = input("INCAR-type Metal or else ? [m/e]: ")
			bulk_or_surf = "s"

	make_incar(os.getcwd(), norm_ck_surf, sysname, cutoff, algo, prec, ediff, spin, scf_or_relax, metal_or_else, bulk_or_surf)

# END: Program
