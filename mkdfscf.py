#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import sys, os, shutil
import subprocess as sub
import addmol.helper as hl
#from pnnl import mkpot as mp

"""
Ljobadd +=  "\nmkldos.py -l -0.2 -h 0.0\n"
Ljobadd += "cd LDOS-from" + "-0.2" + "to" + "0.0" +  "\n"
Ljobadd += "sbatch job.sh\n"
Ljobadd += "cd ..\n"

Ljobadd += "\nmkldos.py -l -0.4 -h -0.2\n"
Ljobadd += "cd LDOS-from" + "-0.4" + "to" + "-0.2" +  "\n"
Ljobadd += "sbatch job.sh\n"
Ljobadd += "cd ..\n"

Ljobadd += "\nmkldos.py -l -0.6 -h -0.4\n"
Ljobadd += "cd LDOS-from" + "-0.6" + "to" + "-0.4" +  "\n"
Ljobadd += "sbatch job.sh\n"
Ljobadd += "cd ..\n"

Ljobadd += "\nmkldos.py -l -0.8 -h -0.6\n"
Ljobadd += "cd LDOS-from" + "-0.8" + "to" + "-0.6" +  "\n"
Ljobadd += "sbatch job.sh\n"
Ljobadd += "cd ..\n"

Ljobadd += "\nmkldos.py -l -1.0 -h -0.8\n"
Ljobadd += "cd LDOS-from" + "-1.0" + "to" + "-0.8" +  "\n"
Ljobadd += "sbatch job.sh\n"
Ljobadd += "cd ..\n"

Ljobadd += "\nmkldos.py -l -1.2 -h -1.0\n"
Ljobadd += "cd LDOS-from" + "-1.2" + "to" + "-1.0" +  "\n"
Ljobadd += "sbatch job.sh\n"
"""

def mkdf_scf(nowpath, direc_ref, flags):
    script = "mkdovasp_tbm.py"
    vasptype = 544
    nodetype = "fn"
    numnode = 1
    #process = 4
    hour = "2:00:00"
    jobname = "scf"
    #ldos_eint_l = -1.0
    #ldos_eint_h = 0.0

    needfiles = ["INCAR","KPOINTS","POTCAR","CONTCAR","WAVECAR","CHGCAR"]
    dict_tag ={"LWAVE =":"LWAVE = T\n",
               "LCHARG =":"LCHARG = T\n",
               "ISTART = 0":"ISTART = 1; ICHARG = 1\n",
               "IBRION =":"IBRION = -1\n",
               "NSW =":"NSW = 0\n",
               "NELM =":"NELM = 1000\n",
               "NELMIN =":"NELMIN = 6\n",
               "NEDOS =":"NEDOS = 3001\n",
               "LVTOT =":"LVTOT = T\n",
               "LAECHG =":"LAECHG = T\n"}

    # Path
    path_ref = nowpath + "/" + direc_ref
    path_scf = nowpath + "/SCF_" + direc_ref
    os.mkdir(path_scf)

    for f in needfiles:
        if not os.path.exists(path_ref+"/"+f):
            if f == "CONTCAR":
                print("CONTCAR does not exist. So POSCAR is copied instead CONTCAR!"); f = "POSCAR"
            else: print(f + " does not exist.You should prepare " + f +"!"); continue

        if f == "CONTCAR":
            shutil.copy(path_ref+"/"+f, path_scf+"/POSCAR")

        elif f == "INCAR":
            with open(path_ref+"/"+f) as fi: incarlines = fi.readlines()

            npar = int([hl.conv_line(l)[2] for l in incarlines if "NPAR =" in l][0])
            kpar = int([hl.conv_line(l)[2] for l in incarlines if "KPAR =" in l][0])
            process = npar * kpar

            L = ""
            for line in incarlines:
                for key in dict_tag:
                    if key == "LVTOT =" and key in line:
                        if flags[0] == "T": L += str(dict_tag[key]); break
                    elif key == "LAECHG =" and key in line:
                        if flags[1] == "T": L += str(dict_tag[key]); break
                    elif key in line: L += str(dict_tag[key]); break
                else: L += line
            with open(path_scf+"/INCAR","w") as fi: fi.write(L)

        else: 
            shutil.copy(path_ref+"/"+f, path_scf)
        print(f)

    if os.path.exists(nowpath + "/job.sh"):
        with open(nowpath + "/job.sh") as fj: joblines = fj.readlines()
        jobname = [l.replace("#$ -N ","").replace("\n","") for l in joblines if "#$ -N" in l][0]
        #print(jobname); sys.exit()

    # Make dovasp
    mkjob= str(script) + " -" + str(vasptype) + " -" + nodetype + " -nn " + str(numnode) + " -np " + str(process) + " -t " + hour + " -n " + jobname
    #Ljobadd = "\nbader CHGCAR && calcbaderchg.py\n"

    os.chdir(path_scf)
    if not os.path.exists(path_scf+"/POTCAR"): 
        sub.call("mkpot.py", shell=True) #mp.mk_pot()
    sub.call(mkjob, shell=True)
    #with open(path_scf + "/job.sh","a") as fj: fj.write(Ljobadd)

if __name__ == "__main__":
    argv = sys.argv

    flags = ["F","F"]
    if len(argv) == 1:
        direc_ref = input("Please input the directory name you want to use files in: ")
        lvtot = input("Do you calculate LOCPOT? [y/N]: ")
        if lvtot in ["y", "Y"]: flags[0] = "T"
        #elif lvtot in["n", "N"]: f_lvtot= "N"
        laechg = input("Do you calculate all electron Bader charge? [y/N]: ")
        if laechg in["y", "Y"]: flags[1] = "T"
        #elif laechg in["n", "N"]: f_laechg = "N"

    else:
        if str(argv[1]) in ["--h" or "-help"]:
            print("\n--h or -help: show HELP")
            print("--rf: input reference directory")
            print("--lp: LVTOT = .TRUE.")
            print("--bd: LAECHG = .TRUE.\n")
            sys.exit()

        if "--rf" in argv: index_r = argv.index("--rf"); direc_ref = argv[index_r +1]

        # LVTOT
        if "--lp" in argv: flags[0] = "T"
        #else: f_lvtot= "N"

        # LAECHG
        if "--bd" in argv: flags[1] = "T"
        #else: f_laechg = "N"

    mkdf_scf(os.getcwd(), direc_ref, flags)
    print("\nDone!\n")
# END: Program