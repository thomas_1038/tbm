#!/usr/bin/env python3                                                            
# -*- coding: utf-8 -*-                                                        

import os
import os.path
import shutil
import sys
import threading

ip_kingshow = "131.112.248.183"

Q_ip = input("Which machine do you send your files? [accel,a / ties,t / ven,v]: ")
if Q_ip in ["accel","a"]:
    ip = "192.168.3.57"
elif Q_ip in ["ties","t"]:
    ip = "192.168.3.58"
elif Q_ip in ["ven","v"]:
    ip = "192.168.3.56"
else:
    print("Please input [accel,a / ties,t / ven,v]. Bye!")
    sys.exit()

filename = input("Please input the name of files you want to send: ")
place_directory = input("Please input the name of directory to which you want to send files: ")

scp = "scp -o 'ProxyCommand ssh nakao@" + ip_kingshow + " nc %h %p' " + filename + " nakao@" + ip + ":" + place_directory
print(scp)
os.system(scp)
