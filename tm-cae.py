#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import addmol.helper as hl
import addmol.readpos as rp
import addmol.mk_poscar as mkp
import calcmulticonf as cmc

def make_tm_cae(nowpath, AddAtom, num_addatom, label):
    vaspfiles = sorted([x for x in os.listdir(nowpath) if os.path.isfile(nowpath+"/"+x) and ".vasp" in x])
    path_backup = nowpath + "/backup"
    if not os.path.exists(path_backup): os.mkdir(path_backup)

    for vf in vaspfiles:
        path_vf = nowpath + "/" + vf
        with open(path_vf) as fp: poslines = fp.readlines()

        labelofel = rp.get_labelofel(poslines)
        numofel = rp.get_numofel(poslines)
        sumofel = rp.get_sumofnumofel(poslines)
        mat_lat = rp.get_matrix(poslines)
        firstposlines, flagS = rp.get_firstlines(poslines)
        coor_car, coor_dir, list_SD = rp.get_coordinate(poslines, sumofel, mat_lat)

        coor_tm = np.array(coor_car[-1])
        coor_tm_z = (coor_tm + np.array([0, 0, 2])).tolist()
        coor_tm_xy1 = (coor_tm + np.array([1.4142, 1.4142, 0])).tolist()
        coor_tm_xy2 = (coor_tm + np.array([1.4142, -1.4142, 0])).tolist()

        label_tm = labelofel[-1]
        labelofel_new = labelofel; labelofel_new.append(AddAtom)
        numofel_new = numofel; numofel_new.append(str(num_addatom))
        eachlabels_el, eachlabels_el_nn, dict_labelofel = rp.mk_labelofel(labelofel_new, numofel_new)

        path_tmp = nowpath + "/tmp"
        path_tm_dir = nowpath + "/" + label_tm
        if not os.path.exists(path_tm_dir): os.mkdir(path_tm_dir)
        #path_models = nowpath + "/models"
        path_models = path_tm_dir + "/models"
        if not os.path.exists(path_models): os.mkdir(path_models)

        print(label_tm)
        for i, l in enumerate([coor_tm_z, coor_tm_xy1, coor_tm_xy2]):
            coor_tm_new = coor_car + [l]
            name_new = "C12A7-" + str(label_tm) + "-" + str(label) + str(i) + ".vasp"
            mkp.mk_pos(path_models,
                       name_new,
                       firstposlines,
                       mat_lat,
                       labelofel_new,
                       numofel_new,
                       eachlabels_el,
                       0,
                       coor_tm_new,
                       [])

        if os.path.exists(path_backup + "/" + path_vf): shutil.move(path_vf, path_backup)
        shutil.copytree(path_tmp, path_tm_dir + "/tmp")
        #cmc.calc_multiconf(path_tm_dir, "-mk")
        #cmc.calc_multiconf(path_tm_dir, "-q")

    mkp.mk_poss(path_models)

if __name__ == "__main__":
    make_tm_cae(os.getcwd(), "O", 1, "O")
    print("\nDone!\n")
# END: Program
