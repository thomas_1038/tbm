#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import os, sys, shutil
from stat import *

vasp_path_541 = "/home/4/17D20121/vasp/vasp.5.4.1_intelmpi_171221/bin/"
vasp_path_544 = "/home/4/17D20121/vasp/vasp.5.4.4_neb_190531/bin/"
jobname = "job.sh"
email = "sakaguchi.t.ad@m.titech.ac.jp"
mailoption = "be" #a: 中止、 b: ジョブ実行、e: ジョブ終了
vasp_path_def = vasp_path_544
node_def = "f_node"
v_type_def = "vasp_gpu"
num_node_def = "1"
job_name_def = "job"
process_def = "8"
time_def = "24:00:00"
#module = "module load cuda/8.0.44 intel intel-mpi fftw"
module = "module load cuda/8.0.61 intel/16.0.4.258 intel-mpi/17.3.196 fftw"

def make_dovasp(node, v_type, num_node, job_name, process, time, vasp_path=vasp_path_544):
    L = "#!/bin/sh\n"
    L += "#$-cwd\n"
    L += "#$ -l " + str(node) + "=" + str(num_node) + "\n"
    L += "#$ -l h_rt=" + str(time) + "\n"
    L += "#$ -N " + str(job_name) + "\n"
    L += "#$ -m " + mailoption + "\n"
    L += "#$ -M " + email + "\n"
    L += ". /etc/profile.d/modules.sh\n"
    L += module + "\n\n"
    L += "vasp_path=\"" + str(vasp_path) + "\"\n"
    L += "vasp_type=\"" + str(v_type) + "\"\n"
    L += "process=" + str(process) + "\n\n"
    L += "echo \"NEW JOB\" >> date.txt\n"
    L += "date >> date.txt\n"
    L += "mpirun -n $process $vasp_path$vasp_type > std.out\n"
    L += "date >> date.txt\n\n"
    return L

def determine_jobcontent_inargv(argv):
    if "-541" in argv:    vasp_path = vasp_path_541
    elif "-544" in argv:  vasp_path = vasp_path_544
    elif "-544n" in argv: vasp_path = vasp_path_544
    # Determine mode
    if "-fn" in argv:   node = "f_node"; v_type = "vasp_gpu"
    elif "-hn" in argv: node = "h_node"; v_type = "vasp_gpu"
    elif "-qn" in argv: node = "q_node"; v_type = "vasp_gpu"
    elif "-sc" in argv: node = "s_core"; v_type = "vasp_std"
    elif "-qc" in argv: node = "q_core"; v_type = "vasp_std"
    elif "-g"  in argv: node = "s_gpu";  v_type = "vasp_gpu"
    else: print("Please input vasp-node! Bye!"); sys.exit()
    # The number of node
    if "-nn" in argv: index_nn = argv.index("-nn"); num_node = argv[index_nn + 1]
    else: num_node = input("Please input the number of node you want to use. e.g., 4; ")
    # Job name
    if "-n" in argv: index_n = argv.index("-n"); job_name = argv[index_n + 1]
    else:
        job_name = input("Please input job name, e.g., test or d, default (" + str(job_name_def) + ") : ")
        if job_name in ["d", "default"]: job_name = job_name_def
    # np
    if "-np" in argv: index_np = argv.index("-np"); process = argv[index_np + 1]
    else:
        process = input("Please input the number of np or default, d (default:" + str(process_def) + "): ")
        if process in ["d", "default"]: process = process_def
    # Job time
    if "-t" in argv: index_t = argv.index("-t"); time = argv[index_t + 1]
    else:
        time = input("Please input the wall time you want to use or [t, test: 00:10:00/ d, default:" + str(time_def) + "]:  ")
        if time in ["d", "default"]: time = time_def
        elif time in ["t", "test"]: time = "00:10:00"
    return vasp_path, node, v_type, num_node, job_name, process, time

def determine_jobcontent_notargv():
    Qvasp_path = input("Please input the type of node, [541, 544, 544n, d ]: ")
    if "541" in Qvasp_path:    vasp_path = vasp_path_541
    elif "544" in Qvasp_path:  vasp_path = vasp_path_544
    elif "544n" in Qvasp_path: vasp_path = vasp_path_544
    elif "d" in Qvasp_path: vasp_path = vasp_path_def

    num_node = input("Please input the number of node you want to use. e.g., 1 or default, d (default: 1); ")
    if num_node in ["d", "default"]: num_node = num_node_def

    node_type = input("Please input the type of node, [f_node, fn / h_node, hn / q_node, qn / s_core, sc / q_core, qc / s_gpu, g  / d, default: f_node]: ")
    if node_type in ["fn", "f_node"]:  node = "f_node";  v_type = "vasp_gpu"
    elif node_type in ["hn", "h_node"]: node = "h_node"; v_type = "vasp_gpu"
    elif node_type in ["qn", "q_node"]: node = "q_node"; v_type = "vasp_gpu"
    elif node_type in ["sc", "s_core"]: node = "s_core"; v_type = "vasp_std"
    elif node_type in ["qc", "q_core"]: node = "q_core"; v_type = "vasp_std"
    elif node_type in ["g", "s_gpu"]:   node = "s_gpu";  v_type = "vasp_gpu"
    elif node_type in ["d", "default"]: node = node_def; v_type = v_type_def

    process = input("Please input the number of np or default, d (default:" + str(process_def) + "): ")
    if process in ["d", "default"]: process = process_def

    time = input("Please input the wall time you want to use. e.g., 1:00:00 or defalt, d (default:" + str(time_def) + ") or test, t (test: 00:10:00); ")
    if time in ["d", "default"]: time = time_def
    elif time in ["t", "test"]: time = "00:10:00"

    job_name = input("Please input job name, e.g., test or defalt, d (default: " + str(job_name_def) + "): ")
    if job_name in ["d", "default"]: job_name = job_name_def
    return vasp_path, node, v_type, num_node, job_name, process, time

if __name__ == "__main__":
    argv = sys.argv
    if len(argv) > 1 and str(argv[1]) in ["-h", "--help"]:
        print("\n-h, --help: show HELP")
        print("-d: make job.sh based on the default.")
        print("vasp_path: " + vasp_path_def)
        print("vasp_type: " + v_type_def)
        print("node_type: " + node_def)
        print("num_node: " + num_node_def)
        print("process: " + process_def)
        print("job_name: " + job_name_def)
        print("\nVASP-version")
        print("-541: vasp.5.4.1\n-544: vasp.5.4.4\n-544n: vasp.5.4.4 for neb")
        print("\nNode-type")
        print("-fn: f_node(cpu:28, gpu:4, memori:240), vasp_gpu")
        print("-hn: h_node(cpu:14, gpu:2, memori:120), vasp_gpu")
        print("-qn: q_node(cpu:7,  gpu:1, memori: 60), vasp_gpu")
        print("-g:  g_node(cpu:2,  gpu:1, memori: 30), vasp_gpu")
        print("-sc: s_core(cpu:1,  gpu:0, memori:7.5), vasp_std")
        print("-qc: q_core(cpu:4,  gpu:0, memori: 30), vasp_std\n")
        print("Other-parameters")
        print("-nn: the number of node\n-n:  the name of job\n-np: the number of np\n-t:  time of calc.\n")
        sys.exit()

    if len(argv) == 2 and "-d" in argv:
        vasp_path = vasp_path_def
        node = node_def
        v_type = v_type_def
        num_node = num_node_def
        job_name = job_name_def
        process = process_def
        time = time_def
    elif len(argv) > 1: vasp_path, node, v_type, num_node, job_name, process, time = determine_jobcontent_inargv(argv)
    elif len(argv) == 1: vasp_path, node, v_type, num_node, job_name, process, time = determine_jobcontent_notargv()

    # make dovasp.sh
    nowpath = os.getcwd()
    L = make_dovasp(node, v_type, num_node, job_name, process, time, vasp_path)
    path_job = nowpath + "/" + jobname
    with open(path_job, "w") as fj: fj.write(L)

    # change the right
    os.chmod(path_job, S_IXUSR | S_IRUSR | S_IWUSR )
    print(str(jobname) + " has been made!")
# END: Program