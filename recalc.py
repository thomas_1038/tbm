#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, datetime, math
import subprocess as sub
import numpy as np
import make.mkbackup as mkb
import addmol.helper as hl
from stat import *

dict_month = {"Jan":1,"Feb":2,"Mar":3,"Apr":4,"May":5,"Jun":6,"Jul":7,"Aug":8,"Sep":9,"Oct":10,"Nov":11,"Dec":12}

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
    while line.count("") > 0: line.remove("")
    return line

flag_req = "reached required accuracy - stopping structural energy minimisation"
#flag_wav = 
path = os.getcwd()

# read Sum_Eads_~.dat
sumEads = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and "Sum_Eads" in x][0]
if len(sumEads) == 1: print("You should prepare Sum_Eads_~.dat! Please use getaden.py. BYE!"); sys.exit()
with open(path + "/" + sumEads) as fes: selines = fes.readlines(); del selines[0]
selines = [conv_line(line.replace("\n","")) for line in selines if line != "\n"]

#Q_group = input("Please input the group you want to use [tga-MCES01, h/ tga-t3mces, t]: ")
#if "h" in Q_group: group = "tga-MCES01"
#elif "t" in Q_group: group = "tga-t3mces"
group = "tga-MCES01"
qsub = "qsub -g " + group + " job.sh"

# set new job-time
jobtime = input("Please input the jobtime you want to use, e.g., 1:00:00, default, d = 24:00:00: ")
if jobtime == "d": jobtime = "24:00:00"

# find the directory calculation is stopping in.
dir_precs = []
for dire in selines:
    precs = sorted([y for y in dire if len(str(y)) == 1])
    #print(precs)

    num_p = 0
    for x in dire:
        #if "Prec" in x:
        if len(str(x)) == 1:
            ind = dire.index(x)
            #print(x)
            if "R" in dire[ind + 1]: 
                num_p += 1
            elif "W" in dire[ind + 1] or "N" in dire[ind + 1]:
                if "Nn" in dire[ind + 1]:
                    tag = "Nn"
                if "W" in dire[ind + 1]: 
                    tag = "W"; dirname = x
                elif "N" in dire[ind + 1]: 
                    tag = "N"; dirname = x

                #if num_p == len(precs) - 1: 
                #    Larray = "array=(\"" + precs[-1] + "\")\n"
                #else:
                #    Larray = "array=("
                #    for x in precs[num_p:]: Larray += "\"" + str(x) + "\" "
                #    else: Larray += ")\n"
                #dir_precs.append([path + "/" + dire[0], dirname, Larray, tag])

                dir_precs.append([path + "/" + dire[0], dirname, tag])
                break

if len(dir_precs) == 0: print("All calculations were already done! BYE!"); sys.exit()
print("\n" + "\n".join(np.array(dir_precs)[:,0].tolist()))
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x)]

print("\n--- resubmit job list ---\n")
for x in dir_precs:
    print("\n"+x[0])
    #path_dir_ad = x[0]; dir_prec = x[1]; Larray = x[2]; tag = x[3]
    path_dir_ad = x[0]; dir_prec = x[1]; tag = x[2]
    #path_dir_prec = path_dir_ad + "/" + dir_prec
    path_job = path_dir_ad + "/job.sh"

    path_dir_prec = path_dir_ad + "/" + [dr for dr in os.listdir(path_dir_ad) if "Prec" in dr and dir_prec + "_" in dr][0]
    path_dir_precs = sorted([dr for dr in os.listdir(path_dir_ad) if "Prec" in dr])[int(dir_prec):]
    Larray = "array=("
    for l in path_dir_precs:
        Larray += "\"" + str(l) + "\" "
    else: Larray += ")\n"

    # rewrite job.sh
    with open(path_job) as fj: joblines = fj.readlines()
    line_array_in_joblines = [line for line in joblines if "array=(" in line and "#" not in line][0]
    joblines[joblines.index(line_array_in_joblines)] = Larray

    jobtime_in_joblines = [line for line in joblines if "#$ -l h_rt=" in line][0]
    joblines[joblines.index(jobtime_in_joblines)] = "#$ -l h_rt=" + jobtime + "\n"
    with open(path_job, "w") as fjn: fjn.write("".join(joblines))

    # make backup-directory
    print("make backup!")
    os.chdir(path_dir_prec)
    sub.call("mkbackup.py", shell=True)

    """
    path_date = path_dir_prec + "/date.txt"
    with open(path_date) as fd: datelines = fd.readlines()
    date_latest = [hl.conv_line(l) for l in datelines[-2:]]
    if tag == "W":
        time = []
        for dl in date_latest:
            hms = dl[3].split(":")
            Y = int(dl[5]); D = int(dl[2]); M = dict_month[dl[1]]
            H = int(hms[0]); Mi = int(hms[1]); S = int(hms[2])
            dt1 = datetime.datetime(year=Y,month=M,day=D,hour=H,minute=Mi,second=S)
            time.append(dt1)
        td = time[1] - time[0]
    """

    path_inc = path_dir_prec + "/INCAR_act"
    with open(path_inc) as fi: incarlines = fi.readlines()

    #path_std = path_dir_prec + "/std.out"
    #with open(path_std) as fs: stdlines = fs.readlines()
    #step_pre = [int(conv_line(l)[0]) for l in stdlines if "F=" in l][-1]

    Li = ""
    for incarline in incarlines:
        if "ISTART = 0" in incarline and "ICHARG = 2" in incarline:
            if tag == "W": Li += "!ISTART = 0; ICHARG = 2\n"
            elif tag == "N": Li += "ISTART = 0; ICHARG = 2\n"
        elif "ISTART = 1" in incarline and "ICHARG = 1" in incarline:
            if tag == "W": Li += "ISTART = 1; ICHARG = 1\n"
            elif tag == "N": Li += "!ISTART = 1; ICHARG = 1\n"

        #elif "NSW =" in incarline: 
        #    step = int(incarline.replace("\n","").replace("NSW = ",""))
        #    if step == 1000: Li += "NSW = 100\n"
        #    elif tag == "N": Li += "NSW = " + str(step_pre) + "\n"
        #    elif tag == "W":
        #        td_sec = td.seconds
        #        step_ideal = math.floor(86400 * step/td_sec)
        #        step_new = step_ideal - math.floor(step_ideal * 0.1) 
        #        Li += "NSW = " + str(step_new) + "\n"

        else: Li += incarline
    with open(path_inc, "w") as fi: fi.write(Li)

    # submit job
    os.chdir(path_dir_ad)
    sub.call(qsub, shell=True)

print("\nDone!\n")
# END: Program
