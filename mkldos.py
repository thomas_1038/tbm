#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/05/01 ver1.2
#2016/06/19 ver1.3
#make LDOS at optional level

import os, sys, shutil
import subprocess as sub
import chgincarnks as cinks
#from pnnl import mkpot as mp

script = "mkdovasp_tbm.py"
vasptype = 544
nodetype = "hn"
numnode = 1
process = 4
jobname = "ldos"
hour = "0:10:00"
range_int_l = -1.0
range_int_h = 0.0
mkjob = script + " -" + str(vasptype) + " -" + nodetype  +  " -nn " + str(numnode) + " -np " + str(process) + " -n " + jobname + " -t " + hour
chgincar = "chgincarnks.py -n 4 -k 1 -s 20"

def mk_ldos(nowpath, argv, mkjob):
    if len(argv) == 1:
        parchg_band_or_dos = input("Do you make PARCHG based on BAND or DOS? [b/d]: ")
        if parchg_band_or_dos == "b":
            band_num = input("Please input the number of the band, n_band starts from 1: ")
            kp = input("Please input the k-point, kp starts from 1: ")

        if parchg_band_or_dos == "d":
            range_EINT_l = input("Please input the lower energy value, for example -1.2: ")
            range_EINT_h = input("Please input the higher energy value, for example 0.0: ")

    if len(argv) > 1 and "-d" in argv: parchg_band_or_dos = "d"
    if len(argv) > 1 and "-bn" in argv: band_num = argv[1 + argv.index("-bn")]
    if len(argv) > 1 and "-kp" in argv: kp = argv[1 + argv.index("-kp")]
    if len(argv) > 1 and "-b" in argv: parchg_band_or_dos = "b"
    if len(argv) > 1 and "-fe" in argv and "-d" in argv: range_EINT_l = range_int_l; range_EINT_h = range_int_h
    if len(argv) > 1 and "-le" in argv and "-d" in argv: range_EINT_l = argv[1 + argv.index("-le")]
    if len(argv) > 1 and "-he" in argv and "-d" in argv: range_EINT_h = argv[1 + argv.index("-he")]

    needfiles = ["CONTCAR","KPOINTS","INCAR","POTCAR","WAVECAR"]

    if parchg_band_or_dos == "d":
        name = "LDOS-" + "from" + str(range_EINT_l) + "to" + str(range_EINT_h)
        dict_incar = {"!LPARD = ":"LPARD = T\n",
                      "!LWAVE = ":"LWAVE = F\n",
                      "LWAVE = ":"LWAVE = F\n",
                      "!NBMOD = ":"NBMOD = -3\n",
                      "!EINT = ":"EINT = " + str(range_EINT_l) + " " + str(range_EINT_h) + "\n",
                      "IBRION = ":"IBRION = -1\n",
                      "NSW = ":"NSW = 0\n",
                      "ISTART = 0 ; ICHARG = 2":"ISTART = 1\n",
                      "ICHARG = 1 ; ISTART = 1":"ISTART = 1\n",
                      "LVTOT = ":"!LVTOT = \n",
                      "NEDOS = ":"!NEDOS = \n",
                      "LAECHG =":"!LAECHG = \n"}

    if parchg_band_or_dos == "b":
        name = "LDOS-" + "kp" + str(kp) + "band" + str(band_num)
        dict_incar = {"!LPARD = ":"LPARD = T\n",
                      "!LWAVE = ":"LWAVE = F\n",
                      "LWAVE = ":"LWAVE = F\n",
                      "!IBAND = ":"IBAND = " + str(band_num) + "\n",
                      "!KPUSE = ":"KPUSE = " + str(kp) + "\n",
                      "IBRION = ":"IBRION = -1\n",
                      "NSW = ":"NSW = 0\n",
                      "ISTART = 0 ; ICHARG = 2":"ISTART = 1\n",
                      "ICHARG = 1 ; ISTART = 1":"ISTART = 1\n",
                      "ICHARG = 11":"ISTART = 1\n",
                      "LVTOT = ":"!LVTOT = \n",
                      "NEDOS = ":"!NEDOS = \n",
                      "LAECHG = ":"!LAECHG = \n"}

    name_curdir = nowpath.split("/")[-2]
    Ljobadd = "rm WAVECAR\n"

    if parchg_band_or_dos == "d":
        Ljobadd += "mv PARCHG " + "PARCHG_" + name_curdir + "_f" + str(range_EINT_l) + "t" + str(range_EINT_h) + ".vasp\n"
    if parchg_band_or_dos == "b":
        Ljobadd += "mv PARCHG " + "PARCHG_" + name_curdir +"_band" + str(band_num) + "_kp" + str(kp) + ".vasp\n"

    path_dir = nowpath + "/" + name
    print("\nmake " + name + "!\n")

    #copy files to LDOS
    for f in needfiles:
        if not os.path.exists(nowpath + "/" + f):
            if f == "WAVECAR": print(f + " doesn't exist! You should prepare it! BYE!"); sys.exit()
            elif f == "INCAR": print(f + " doesn't exist! You should prepare it! BYE!"); sys.exit()

    os.mkdir(path_dir)
    for f in needfiles:
        if not os.path.exists(nowpath + "/" + f):
            if f == "CONTCAR": print(f + " doesn't exist! POSCAR is copied instead CONTCAR."); f = "POSCAR"
            else: print(f + " doesn't exist!"); continue

        print(f)
        if f == "CONTCAR": shutil.copy(nowpath+"/"+f, path_dir+"/POSCAR")
    #    elif f == "WAVECAR": shutil.move(path+"/"+f, path_dir)
        elif f == "INCAR":
            with open(nowpath + "/" + f) as fi: incarlines = fi.readlines()

            L = ""
            for line in incarlines:
                for key in dict_incar:
                    if key in line: L += dict_incar[key]; break
                else: L += line
            with open(path_dir+"/INCAR","w") as fi: fi.write(L)
        else: shutil.copy(nowpath+"/"+f, path_dir)

    #shfiles = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and ".sh" in x]
    #if len(shfiles) > 0: shutil.copy(path + "/" + str(shfiles[0]), path_dir)
    os.chdir(path_dir)
    if not os.path.exists(path_dir+"/POTCAR"): sub.call("mkpot.py", shell=True) #mp.mk_pot()
    sub.call(mkjob, shell=True)
    with open(path_dir + "/job.sh","a") as fj: fj.write(Ljobadd)
    cinks.conv_INCAR(path_dir, "INCAR")

if __name__ == "__main__":
    mk_ldos(os.getcwd(), sys.argv, mkjob); print("\nDONE!\n")
# END: Program
