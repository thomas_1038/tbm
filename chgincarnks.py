#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil, threading

def conv_INCAR(nowpath, incar, npar=4, kpar=1, nsim=24):
    path_INCAR = nowpath + "/" + incar
    with open(path_INCAR) as fin: incarlines = fin.readlines()

    num = 0
    while num < len(incarlines):
        if incarlines[num].find("NPAR =") >= 0:   incarlines[num] = "NPAR = " + str(npar) + "\n"
        elif incarlines[num].find("KPAR =") >= 0: incarlines[num] = "KPAR = " + str(kpar) + "\n"
        elif incarlines[num].find("NSIM =") >= 0: incarlines[num] = "NSIM = " + str(nsim) + "\n"
        num += 1

        # change INCAR
        with open(path_INCAR, "w") as fin: fin.write("".join(incarlines))

if __name__ == "_main__": 
    Q_INCAR = input("Please input [default,d / test,t / acutual,a ] or other INCAR name: ")
    if Q_INCAR in ["d"]: INCARname = "INCAR"
    elif Q_INCAR in ["test","t"]: INCARname = "INCAR_test"
    elif Q_INCAR in ["act","a"]:  INCARname = "INCAR_act"
    else: INCARname = Q_INCAR

    argv = sys.argv
    if len(argv) > 1:
        if "-n" in argv: index_n = argv.index("-n"); npar = str(argv[index_n + 1])
        if "-k" in argv: index_k = argv.index("-k"); kpar = str(argv[index_k + 1])
        if "-s" in argv: index_s = argv.index("-s"); nsim = str(argv[index_s + 1])

    elif len(argv) == 1:
        npar = input("Please input NPAR value, e.g., 4: ")
        kpar = input("Please input KPAR value, e.g., 1: ")
        nsim = input("Please input NSIM value, e.g., 12: ")

    conv_INCAR(os.getcwd(), INCARname, npar, kpar, nsim)    
    print("Done!")
# END: Program